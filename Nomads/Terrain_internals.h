#ifndef TERRAIN_INTERNALS_H
#define TERRAIN_INTERNALS_H

#ifdef __cplusplus
extern "C" {
#endif

#include "TerrainDescriptor.h"
#include "TerrainCommon_internals.h"
#include "TreeTerrain_internals.h"
#include "BushTerrain_internals.h"
#include "RockTerrain_internals.h"
#include "FlintrockTerrain_internals.h"
#include "WaterTerrain_internals.h"
#include "BerryStarterTerrain_internals.h"
#include "DirtTerrain_internals.h"
#include "GrassTerrain_internals.h"
#include "AcornTerrain_internals.h"

	typedef struct RabbitHoleTerrain {
		TERRAINCOMMON common;
		int rabbitTimer;
	} RABBITHOLETERRAIN;

	union Terrain {
		TERRAINCOMMON common;
		TREETERRAIN tree;
		ROCKTERRAIN rock;
		FLINTROCKTERRAIN flintrock;
		BUSHTERRAIN bush;
		RABBITHOLETERRAIN rabbitHole;
		WATERTERRAIN water;
		BERRYSTARTERTERRAIN berryStarter;
		DIRTTERRAIN dirt;
		ACORNTERRAIN acorn;
	};

#ifdef __cplusplus
}
#endif

#endif

