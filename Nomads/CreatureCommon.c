#include "CreatureCommon_internals.h"
#include <assert.h>

CREATURETYPE CreatureCommon_getType(CREATURECOMMON* ptr)
{
	assert(ptr);
	return ptr->type;
}
