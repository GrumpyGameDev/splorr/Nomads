#include "View_internals.h"
#include "PixelPlotter_internals.h"
#include "PatternPlotter.h"
#include "Views.h"
#include "Game.h"
#include "Constants.h"
#include "TerrainDescriptors.h"
#include "CreatureDescriptors.h"
#include "ItemDescriptors.h"
#include "Direction.h"
#include <assert.h>
#include "Utility.h"
#include <stdio.h>
#include "TextPlotter.h"
#include "Recipes.h"
#include <string.h>

#define INVENTORYSCREEN_OFFSET_X (0)
#define INVENTORYSCREEN_OFFSET_Y (0)
#define INVENTORYSCREEN_WIDTH (SCALE_WIDTH(SCALE_SMALL))
#define INVENTORYSCREEN_HEIGHT (SCALE_HEIGHT(SCALE_SMALL))
#define INVENTORYSCREEN_ROWS ((INVENTORYSCREEN_HEIGHT)/(PATTERN_HEIGHT))
#define INVENTORYSCREEN_COLUMNS  ((INVENTORYSCREEN_WIDTH)/(PATTERN_WIDTH))

#define INVENTORYSCREEN_IN_HAND_OFFSET_X (INVENTORYSCREEN_OFFSET_X)
#define INVENTORYSCREEN_IN_HAND_OFFSET_Y (INVENTORYSCREEN_OFFSET_Y + PATTERN_HEIGHT)

#define INVENTORYSCREEN_ON_GROUND_OFFSET_X (INVENTORYSCREEN_OFFSET_X + INVENTORYSCREEN_COLUMNS * PATTERN_WIDTH * 1 / 2)
#define INVENTORYSCREEN_ON_GROUND_OFFSET_Y (INVENTORYSCREEN_OFFSET_Y + PATTERN_HEIGHT)

#define INVENTORYSCREEN_SCROLLER_LIST_SIZE (INVENTORYSCREEN_ROWS - 3)
#define INVENTORYSCREEN_SCROLLER_HEIGHT (INVENTORYSCREEN_SCROLLER_LIST_SIZE * PATTERN_HEIGHT)

void InventoryScreen_initialize(union View* ptr)
{
	assert(ptr);
	PixelPlotterState_initialize(View_getPixelPlotterState(ptr),
		INVENTORYSCREEN_OFFSET_X,
		INVENTORYSCREEN_OFFSET_Y,
		SCALE_SMALL,
		SCALE_SMALL,
		1,
		INVENTORYSCREEN_WIDTH,
		INVENTORYSCREEN_HEIGHT);
	ptr->craftScreen.viewCommon.viewType = VIEW_INVENTORY_SCREEN;
	ptr->craftScreen.viewCommon.enabled = 0;

	for (INVENTORYSCREENSCROLLERTYPE scroller = INVENTORYSCREENSCROLLER_FIRST; scroller < INVENTORYSCREENSCROLLER_COUNT; ++scroller)
	{
		Scroller_initialize(InventoryScreen_getScroller(View_getInventoryScreen(ptr), scroller), 0, INVENTORYSCREEN_SCROLLER_LIST_SIZE);
	}

	ptr->craftScreen.currentScroller = INVENTORYSCREENSCROLLER_FIRST;
}

void InventoryScreen_draw(union View* ptr)
{
	assert(ptr);
	PIXELPLOTTERSTATE old;
	PixelPlotter_getState(&old);
	PixelPlotter_setState(View_getPixelPlotterState(ptr));
	char buffer[10];

	PixelPlotter_plotRect(0, 0, INVENTORYSCREEN_WIDTH, INVENTORYSCREEN_HEIGHT, COLOR_DARK_SILVER);
	TextPlotter_plotText(INVENTORYSCREEN_IN_HAND_OFFSET_X, 0, "Inventory:", COLOR_SILVER, COLOR_TRANSPARENT);
	TextPlotter_plotText(INVENTORYSCREEN_ON_GROUND_OFFSET_X, 0, "On Ground:", COLOR_SILVER, COLOR_TRANSPARENT);

	TextPlotter_plotText(0, INVENTORYSCREEN_HEIGHT - PATTERN_HEIGHT * 2, "  back    select ,. switch", COLOR_SILVER, COLOR_TRANSPARENT);
	TextPlotter_plotText(0, INVENTORYSCREEN_HEIGHT - PATTERN_HEIGHT * 1, "  take/drop all X take/drop one", COLOR_SILVER, COLOR_TRANSPARENT);

	TextPlotter_plotText(0 * PATTERN_WIDTH, INVENTORYSCREEN_HEIGHT - PATTERN_HEIGHT * 2, "Q", COLOR_DARK_SILVER, COLOR_SILVER);
	TextPlotter_plotText(7 * PATTERN_WIDTH, INVENTORYSCREEN_HEIGHT - PATTERN_HEIGHT * 2, "WS", COLOR_DARK_SILVER, COLOR_SILVER);
	TextPlotter_plotText(17 * PATTERN_WIDTH, INVENTORYSCREEN_HEIGHT - PATTERN_HEIGHT * 2, ",.", COLOR_DARK_SILVER, COLOR_SILVER);

	TextPlotter_plotText(0 * PATTERN_WIDTH, INVENTORYSCREEN_HEIGHT - PATTERN_HEIGHT * 1, "E", COLOR_DARK_SILVER, COLOR_SILVER);
	TextPlotter_plotText(16 * PATTERN_WIDTH, INVENTORYSCREEN_HEIGHT - PATTERN_HEIGHT * 1, "X", COLOR_DARK_SILVER, COLOR_SILVER);


	INVENTORYSCREEN* craftScreen = View_getInventoryScreen(ptr);
	SCROLLER* inventoryScroller = InventoryScreen_getScroller(craftScreen, INVENTORYSCREENSCROLLER_IN_HAND);
	TAGON* tagon = Game_getTagon();
	if (Scroller_getItemCount(inventoryScroller) == 0)
	{
		TextPlotter_plotText(INVENTORYSCREEN_IN_HAND_OFFSET_X, PATTERN_HEIGHT, "(nothing)", COLOR_SILVER, COLOR_TRANSPARENT);
	}
	//draw in hand inventory
	for (int row = 0, plotY = INVENTORYSCREEN_IN_HAND_OFFSET_Y; row < Utility_minimum(INVENTORYSCREEN_ROWS, Scroller_getItemCount(inventoryScroller)); ++row, plotY += PATTERN_HEIGHT)
	{
		int plotX = INVENTORYSCREEN_IN_HAND_OFFSET_X;
		PatternPlotter_plotPattern(plotX, plotY, PATTERN_EMPTY, COLOR_ONYX, COLOR_ONYX);
		int effectiveRow = row + Scroller_getScroll(inventoryScroller);
		if (effectiveRow == Scroller_getIndex(inventoryScroller))
		{
			PatternPlotter_plotPattern(plotX, plotY, PATTERN_BOX, (InventoryScreen_getCurrentScroller(craftScreen) == INVENTORYSCREENSCROLLER_IN_HAND) ? (COLOR_LIGHTEST) : (COLOR_DARKEST), COLOR_TRANSPARENT);
		}
		ITEMTYPE item = ptr->craftScreen.inventoryItems[effectiveRow];
		ITEMDESCRIPTOR* descriptor = ItemDescriptors_getDescriptor(item);
		PatternPlotter_plotPattern(plotX, plotY, ItemDescriptor_getPattern(descriptor), ItemDescriptor_getForeground(descriptor), ItemDescriptor_getBackground(descriptor));
		snprintf(buffer, 10, "x%d", Tagon_getInventoryCount(tagon, item));
		TextPlotter_plotText(plotX + PATTERN_WIDTH, plotY, buffer, COLOR_SILVER, COLOR_TRANSPARENT);
	}

	inventoryScroller = InventoryScreen_getScroller(craftScreen, INVENTORYSCREENSCROLLER_ON_GROUND);
	INVENTORY* inventory = Game_getGroundInventory();
	if (Scroller_getItemCount(inventoryScroller) == 0)
	{
		TextPlotter_plotText(INVENTORYSCREEN_ON_GROUND_OFFSET_X, PATTERN_HEIGHT, "(nothing)", COLOR_SILVER, COLOR_TRANSPARENT);
	}
	//draw on ground inventory
	for (int row = 0, plotY = INVENTORYSCREEN_ON_GROUND_OFFSET_Y; row < Utility_minimum(INVENTORYSCREEN_ROWS, Scroller_getItemCount(inventoryScroller)); ++row, plotY += PATTERN_HEIGHT)
	{
		int plotX = INVENTORYSCREEN_ON_GROUND_OFFSET_X;
		PatternPlotter_plotPattern(plotX, plotY, PATTERN_EMPTY, COLOR_ONYX, COLOR_ONYX);
		int effectiveRow = row + Scroller_getScroll(inventoryScroller);
		if (effectiveRow == Scroller_getIndex(inventoryScroller))
		{
			PatternPlotter_plotPattern(plotX, plotY, PATTERN_BOX, (InventoryScreen_getCurrentScroller(craftScreen) == INVENTORYSCREENSCROLLER_IN_HAND) ? (COLOR_LIGHTEST) : (COLOR_DARKEST), COLOR_TRANSPARENT);
		}
		ITEMTYPE item = ptr->craftScreen.inventoryItems[effectiveRow];
		ITEMDESCRIPTOR* descriptor = ItemDescriptors_getDescriptor(item);
		PatternPlotter_plotPattern(plotX, plotY, ItemDescriptor_getPattern(descriptor), ItemDescriptor_getForeground(descriptor), ItemDescriptor_getBackground(descriptor));
		snprintf(buffer, 10, "x%d", Inventory_getCount(inventory, item));
		TextPlotter_plotText(plotX + PATTERN_WIDTH, plotY, buffer, COLOR_SILVER, COLOR_TRANSPARENT);
	}

	PixelPlotter_setState(&old);
}

int InventoryScreen_process(union View* ptr, VIEWCOMMANDTYPE command)
{
	assert(ptr);
	INVENTORYSCREEN* inventoryScreen = View_getInventoryScreen(ptr);
	TAGON* tagon = Game_getTagon();
	switch (command)
	{
	case VIEWCOMMAND_BUTTON_B:
		Tagon_postInteract(tagon);
		Views_disable(VIEW_INVENTORY_SCREEN);
		return 1;
	default:
		return 1;
	}
}

void InventoryScreen_refresh(INVENTORYSCREEN* ptr)
{
	assert(ptr);
	INVENTORY* inventory = Tagon_getInventory(Game_getTagon());
	//refresh in hand
	ITEMTYPE inventoryIndex = ITEM_FIRST;
	for (ITEMTYPE iterator = ITEM_FIRST; iterator < ITEM_COUNT; ++iterator)
	{
		if (Inventory_hasCount(inventory, iterator))
		{
			ptr->inHand[inventoryIndex++] = iterator;
		}
	}
	Scroller_setItemCount(InventoryScreen_getScroller(ptr, INVENTORYSCREENSCROLLER_IN_HAND), inventoryIndex);

	//refresh on ground
	inventoryIndex = ITEM_FIRST;
	inventory = Game_getGroundInventory();
	for (ITEMTYPE iterator = ITEM_FIRST; iterator < ITEM_COUNT; ++iterator)
	{
		if (Inventory_hasCount(inventory, iterator))
		{
			ptr->onGround[inventoryIndex++] = iterator;
		}
	}
	Scroller_setItemCount(InventoryScreen_getScroller(ptr, INVENTORYSCREENSCROLLER_ON_GROUND), inventoryIndex);
}

SCROLLER* InventoryScreen_getScroller(INVENTORYSCREEN* ptr, INVENTORYSCREENSCROLLERTYPE scrollerType)
{
	assert(ptr);
	assert(scrollerType >= INVENTORYSCREENSCROLLER_FIRST && scrollerType < INVENTORYSCREENSCROLLER_COUNT);
	return &(ptr->scrollers[scrollerType]);
}

INVENTORYSCREENSCROLLERTYPE InventoryScreen_getCurrentScroller(INVENTORYSCREEN* ptr)
{
	assert(ptr);
	return ptr->currentScroller;
}

void InventoryScreen_toggleCurrentScroller(INVENTORYSCREEN* ptr)
{
	assert(ptr);
	ptr->currentScroller = (ptr->currentScroller + 1) % INVENTORYSCREENSCROLLER_COUNT;
}

