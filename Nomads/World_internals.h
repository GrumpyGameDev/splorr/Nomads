#ifndef WORLD_INTERNALS_H
#define WORLD_INTERNALS_H

#ifdef __cplusplus
extern "C" {
#endif

#include "Avatar.h"
#include "Map_internals.h"
#include "Avatar_internals.h"
#include "MessageFeed.h"

struct World {
	AVATAR avatar;
	MAP map;
	MESSAGEFEED messageFeed;
};

#ifdef __cplusplus
}
#endif

#endif

