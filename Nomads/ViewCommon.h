#ifndef VIEW_COMMON_H
#define VIEW_COMMON_H

#ifdef __cplusplus
extern "C" {
#endif

#include "ViewType.h"
#include "PixelPlotter.h"

typedef struct ViewCommon VIEWCOMMON;

void ViewCommon_enable(VIEWCOMMON* ptr);
void ViewCommon_disable(VIEWCOMMON* ptr);
int ViewCommon_isEnabled(VIEWCOMMON* ptr);

VIEWTYPE ViewCommon_getType(VIEWCOMMON* ptr);
PIXELPLOTTERSTATE* ViewCommon_getPixelPlotterState(VIEWCOMMON* ptr);

#ifdef __cplusplus
}
#endif

#endif

