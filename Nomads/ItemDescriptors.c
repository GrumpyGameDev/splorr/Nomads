#include "ItemDescriptors.h"
#include "ItemDescriptor_internals.h"
#include <string.h>
#include <assert.h>

static ITEMDESCRIPTOR itemDescriptors[ITEM_COUNT+1] = { 0 };

typedef void(*ItemDescriptorInitializFunc)(ITEMDESCRIPTOR*);

static void initializeLog(ITEMDESCRIPTOR* ptr)
{
	assert(ptr);
	ptr->itemCommon.common.type.itemType = ITEM_LOG;
	ptr->itemCommon.common.pattern = PATTERN_LOG;
	ptr->itemCommon.common.foreground = COLOR_DARK_COPPER;
	ptr->itemCommon.common.background = COLOR_TRANSPARENT;
	ptr->itemCommon.name = "Log";
}

static void initializeStone(ITEMDESCRIPTOR* ptr)
{
	assert(ptr);
	ptr->itemCommon.common.type.itemType = ITEM_STONE;
	ptr->itemCommon.common.pattern = PATTERN_STONE;
	ptr->itemCommon.common.foreground = COLOR_DARK_SILVER;
	ptr->itemCommon.common.background = COLOR_TRANSPARENT;
	ptr->itemCommon.name = "Stone";
}

static void initializeFlint(ITEMDESCRIPTOR* ptr)
{
	assert(ptr);
	ptr->itemCommon.common.type.itemType = ITEM_FLINT;
	ptr->itemCommon.common.pattern = PATTERN_STONE;
	ptr->itemCommon.common.foreground = COLOR_DARKER_SILVER;
	ptr->itemCommon.common.background = COLOR_TRANSPARENT;
	ptr->itemCommon.name = "Flint";
}

static void initializeFish(ITEMDESCRIPTOR* ptr)
{
	assert(ptr);
	ptr->itemCommon.common.type.itemType = ITEM_FISH;
	ptr->itemCommon.common.pattern = PATTERN_FISH;
	ptr->itemCommon.common.foreground = COLOR_DARK_COPPER;
	ptr->itemCommon.common.background = COLOR_TRANSPARENT;
	ptr->itemCommon.name = "Fish";
}

static void initializeWoodAxe(ITEMDESCRIPTOR* ptr)
{
	assert(ptr);
	ptr->itemCommon.common.type.itemType = ITEM_WOOD_AXE;
	ptr->itemCommon.common.pattern = PATTERN_AXE;
	ptr->itemCommon.common.foreground = COLOR_DARK_COPPER;
	ptr->itemCommon.common.background = COLOR_TRANSPARENT;
	ptr->itemCommon.name = "Wood Axe";
}

static void initializeStoneAxe(ITEMDESCRIPTOR* ptr)
{
	assert(ptr);
	ptr->itemCommon.common.type.itemType = ITEM_STONE_AXE;
	ptr->itemCommon.common.pattern = PATTERN_AXE;
	ptr->itemCommon.common.foreground = COLOR_DARK_SILVER;
	ptr->itemCommon.common.background = COLOR_TRANSPARENT;
	ptr->itemCommon.name = "Stone Axe";
}

static void initializeWoodPick(ITEMDESCRIPTOR* ptr)
{
	assert(ptr);
	ptr->itemCommon.common.type.itemType = ITEM_WOOD_PICK;
	ptr->itemCommon.common.pattern = PATTERN_PICKAXE;
	ptr->itemCommon.common.foreground = COLOR_DARK_COPPER;
	ptr->itemCommon.common.background = COLOR_TRANSPARENT;
	ptr->itemCommon.name = "Wood Pick";
}

static void initializeStonePick(ITEMDESCRIPTOR* ptr)
{
	assert(ptr);
	ptr->itemCommon.common.type.itemType = ITEM_STONE_PICK;
	ptr->itemCommon.common.pattern = PATTERN_PICKAXE;
	ptr->itemCommon.common.foreground = COLOR_DARK_SILVER;
	ptr->itemCommon.common.background = COLOR_TRANSPARENT;
	ptr->itemCommon.name = "Stone Pick";
}

static void initializeWoodShovel(ITEMDESCRIPTOR* ptr)
{
	assert(ptr);
	ptr->itemCommon.common.type.itemType = ITEM_WOOD_SHOVEL;
	ptr->itemCommon.common.pattern = PATTERN_SHOVEL;
	ptr->itemCommon.common.foreground = COLOR_DARK_COPPER;
	ptr->itemCommon.common.background = COLOR_TRANSPARENT;
	ptr->itemCommon.name = "Wood Shovel";
}

static void initializeStoneShovel(ITEMDESCRIPTOR* ptr)
{
	assert(ptr);
	ptr->itemCommon.common.type.itemType = ITEM_STONE_SHOVEL;
	ptr->itemCommon.common.pattern = PATTERN_SHOVEL;
	ptr->itemCommon.common.foreground = COLOR_DARK_SILVER;
	ptr->itemCommon.common.background = COLOR_TRANSPARENT;
	ptr->itemCommon.name = "Stone Shovel";
}

static void initializeWoodSpear(ITEMDESCRIPTOR* ptr)
{
	assert(ptr);
	ptr->itemCommon.common.type.itemType = ITEM_WOOD_SPEAR;
	ptr->itemCommon.common.pattern = PATTERN_SPEAR;
	ptr->itemCommon.common.foreground = COLOR_DARK_COPPER;
	ptr->itemCommon.common.background = COLOR_TRANSPARENT;
	ptr->itemCommon.name = "Wood Spear";
}

static void initializeStoneSpear(ITEMDESCRIPTOR* ptr)
{
	assert(ptr);
	ptr->itemCommon.common.type.itemType = ITEM_STONE_SPEAR;
	ptr->itemCommon.common.pattern = PATTERN_SPEAR;
	ptr->itemCommon.common.foreground = COLOR_DARK_SILVER;
	ptr->itemCommon.common.background = COLOR_TRANSPARENT;
	ptr->itemCommon.name = "Stone Spear";
}

static void initializeNone(ITEMDESCRIPTOR* ptr)
{
	assert(ptr);
	ptr->itemCommon.common.type.itemType = ITEM_NONE;
	ptr->itemCommon.common.pattern = PATTERN_EMPTY;
	ptr->itemCommon.common.foreground = COLOR_TRANSPARENT;
	ptr->itemCommon.common.background = COLOR_TRANSPARENT;
	ptr->itemCommon.name = "None";
}

static void initializeCarnelianBerry(ITEMDESCRIPTOR* ptr)
{
	assert(ptr);
	ptr->itemCommon.common.type.itemType = ITEM_CARNELIAN_BERRY;
	ptr->itemCommon.common.pattern = PATTERN_BERRIES;
	ptr->itemCommon.common.foreground = COLOR_CARNELIAN;
	ptr->itemCommon.common.background = COLOR_TRANSPARENT;
	ptr->berry.berryType = BERRY_CARNELIAN;
	ptr->berry.energy = 5;
	ptr->itemCommon.name = "Carberry";
}

static void initializeCopperBerry(ITEMDESCRIPTOR* ptr)
{
	assert(ptr);
	ptr->itemCommon.common.type.itemType = ITEM_COPPER_BERRY;
	ptr->itemCommon.common.pattern = PATTERN_BERRIES;
	ptr->itemCommon.common.foreground = COLOR_COPPER;
	ptr->itemCommon.common.background = COLOR_TRANSPARENT;
	ptr->berry.berryType = BERRY_COPPER;
	ptr->berry.energy = 10;
	ptr->itemCommon.name = "Copberry";
}

static void initializeGoldBerry(ITEMDESCRIPTOR* ptr)
{
	assert(ptr);
	ptr->itemCommon.common.type.itemType = ITEM_GOLD_BERRY;
	ptr->itemCommon.common.pattern = PATTERN_BERRIES;
	ptr->itemCommon.common.foreground = COLOR_GOLD;
	ptr->itemCommon.common.background = COLOR_TRANSPARENT;
	ptr->berry.berryType = BERRY_GOLD;
	ptr->berry.energy = 15;
	ptr->itemCommon.name = "Golberry";
}

static void initializeJadeBerry(ITEMDESCRIPTOR* ptr)
{
	assert(ptr);
	ptr->itemCommon.common.type.itemType = ITEM_JADE_BERRY;
	ptr->itemCommon.common.pattern = PATTERN_BERRIES;
	ptr->itemCommon.common.foreground = COLOR_JADE;
	ptr->itemCommon.common.background = COLOR_TRANSPARENT;
	ptr->berry.berryType = BERRY_JADE;
	ptr->berry.energy = 20;
	ptr->itemCommon.name = "Jadberry";
}

static void initializeTurquoiseBerry(ITEMDESCRIPTOR* ptr)
{
	assert(ptr);
	ptr->itemCommon.common.type.itemType = ITEM_TURQUOISE_BERRY;
	ptr->itemCommon.common.pattern = PATTERN_BERRIES;
	ptr->itemCommon.common.foreground = COLOR_TURQUOISE;
	ptr->itemCommon.common.background = COLOR_TRANSPARENT;
	ptr->berry.berryType = BERRY_TURQUOISE;
	ptr->berry.energy = 25;
	ptr->itemCommon.name = "Turberry";
}

static void initializeRubyBerry(ITEMDESCRIPTOR* ptr)
{
	assert(ptr);
	ptr->itemCommon.common.type.itemType = ITEM_RUBY_BERRY;
	ptr->itemCommon.common.pattern = PATTERN_BERRIES;
	ptr->itemCommon.common.foreground = COLOR_RUBY;
	ptr->itemCommon.common.background = COLOR_TRANSPARENT;
	ptr->berry.berryType = BERRY_RUBY;
	ptr->berry.energy = 30;
	ptr->itemCommon.name = "Ruberry";
}

static void initializeAmethystBerry(ITEMDESCRIPTOR* ptr)
{
	assert(ptr);
	ptr->itemCommon.common.type.itemType = ITEM_AMETHYST_BERRY;
	ptr->itemCommon.common.pattern = PATTERN_BERRIES;
	ptr->itemCommon.common.foreground = COLOR_AMETHYST;
	ptr->itemCommon.common.background = COLOR_TRANSPARENT;
	ptr->berry.berryType = BERRY_AMETHYST;
	ptr->berry.energy = 35;
	ptr->itemCommon.name = "Amberry";
}

static void initializeSilverBerry(ITEMDESCRIPTOR* ptr)
{
	assert(ptr);
	ptr->itemCommon.common.type.itemType = ITEM_SILVER_BERRY;
	ptr->itemCommon.common.pattern = PATTERN_BERRIES;
	ptr->itemCommon.common.foreground = COLOR_SILVER;
	ptr->itemCommon.common.background = COLOR_TRANSPARENT;
	ptr->berry.berryType = BERRY_SILVER;
	ptr->berry.energy = 40;
	ptr->itemCommon.name = "Silberry";
}

static void initializeBerrySeed(ITEMDESCRIPTOR* ptr)
{
	assert(ptr);
	ptr->itemCommon.common.type.itemType = ITEM_BERRY_SEED;
	ptr->itemCommon.common.pattern = PATTERN_BERRYSEED;
	ptr->itemCommon.common.foreground = COLOR_DARK_COPPER;
	ptr->itemCommon.common.background = COLOR_TRANSPARENT;
	ptr->itemCommon.name = "Berry Seed";
}

static void initializeAcorn(ITEMDESCRIPTOR* ptr)
{
	assert(ptr);
	ptr->itemCommon.common.type.itemType = ITEM_ACORN;
	ptr->itemCommon.common.pattern = PATTERN_ACORN;
	ptr->itemCommon.common.foreground = COLOR_DARK_COPPER;
	ptr->itemCommon.common.background = COLOR_TRANSPARENT;
	ptr->itemCommon.name = "Acorn";
}

static void initializeGravel(ITEMDESCRIPTOR* ptr)
{
	assert(ptr);
	ptr->itemCommon.common.type.itemType = ITEM_GRAVEL;
	ptr->itemCommon.common.pattern = PATTERN_PILE;
	ptr->itemCommon.common.foreground = COLOR_DARK_SILVER;
	ptr->itemCommon.common.background = COLOR_TRANSPARENT;
	ptr->itemCommon.name = "Gravel";
}

static ItemDescriptorInitializFunc itemDescriptorInitializeFuncs[ITEM_COUNT+1] =
{
	initializeLog,
	initializeStone,
	initializeFlint,
	initializeFish,
	initializeWoodAxe,
	initializeStoneAxe,
	initializeWoodPick,
	initializeStonePick,
	initializeWoodShovel,
	initializeStoneShovel,
	initializeWoodSpear,
	initializeStoneSpear,
	initializeCarnelianBerry,
	initializeCopperBerry,
	initializeGoldBerry,
	initializeJadeBerry,
	initializeTurquoiseBerry,
	initializeRubyBerry,
	initializeAmethystBerry,
	initializeSilverBerry,
	initializeBerrySeed,
	initializeAcorn,
	initializeGravel,
	initializeNone
};

void ItemDescriptors_initialize(void)
{
	for (int index = 0; index <= ITEM_COUNT; ++index)
	{
		itemDescriptorInitializeFuncs[index](ItemDescriptors_getDescriptor(index));
	}
}

ITEMDESCRIPTOR* ItemDescriptors_getDescriptor(ITEMTYPE itemType)
{
	assert(itemType>=ITEM_FIRST && itemType<=ITEM_COUNT);
	return &itemDescriptors[itemType];
}

const char* ItemDescriptors_getName(ITEMTYPE itemType)
{
	return ItemDescriptor_getName(ItemDescriptors_getDescriptor(itemType));
}