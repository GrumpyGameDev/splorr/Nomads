#ifndef CREATURE_DESCRIPTORS_H
#define CREATURE_DESCRIPTORS_H

#ifdef __cplusplus
extern "C" {
#endif

#include "CreatureDescriptor.h"
#include "CreatureType.h"

void CreatureDescriptors_initialize(void);
CREATUREDESCRIPTOR* CreatureDescriptors_getDescriptor(CREATURETYPE creatureType);

#ifdef __cplusplus
}
#endif

#endif
