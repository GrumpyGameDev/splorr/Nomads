#ifndef ITEM_DESCRIPTORS_H
#define ITEM_DESCRIPTORS_H

#ifdef __cplusplus
extern "C" {
#endif

#include "ItemDescriptor.h"
#include "ItemType.h"

void ItemDescriptors_initialize(void);
ITEMDESCRIPTOR* ItemDescriptors_getDescriptor(ITEMTYPE itemType);
const char* ItemDescriptors_getName(ITEMTYPE itemType);

#ifdef __cplusplus
}
#endif

#endif

