#ifndef GRASS_TERRAIN_INTERNALS_H
#define GRASS_TERRAIN_INTERNALS_H

#ifdef __cplusplus
extern "C" {
#endif

#include "GrassTerrain.h"
#include "TerrainCommon_internals.h"

	union TerrainDescriptor;
	union Terrain;

	void GrassTerrain_initialize(union Terrain* ptr, union TerrainDescriptor* descriptor);
	void StumpTerrain_initialize(union Terrain* ptr, union TerrainDescriptor* descriptor);//TODO: move me
	void RubbleTerrain_initialize(union Terrain* ptr, union TerrainDescriptor* descriptor);//TODO: move me
	void RabbitHole_initialize(union Terrain* ptr, union TerrainDescriptor* descriptor);//TODO: move me



#ifdef __cplusplus
}
#endif

#endif

