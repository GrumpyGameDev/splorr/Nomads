#ifndef AVATAR_INTERNALS_H
#define AVATAR_INTERNALS_H

#ifdef __cplusplus
extern "C" {
#endif

#include "Avatar.h"

	struct Avatar {
		int x;
		int y;
		DIRECTIONTYPE facing;
	};

#ifdef __cplusplus
}
#endif

#endif

