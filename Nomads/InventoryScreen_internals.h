#ifndef INVENTORY_SCREEN_INTERNALS_H
#define INVENTORY_SCREEN_INTERNALS_H

#ifdef __cplusplus
extern "C" {
#endif

#include "InventoryScreen.h"
#include "ViewCommon_internals.h"
#include "RecipeType.h"
#include "ItemType.h"
#include "Scroller_internals.h"

	struct InventoryScreen {
		VIEWCOMMON viewCommon;
		ITEMTYPE inHand[ITEM_COUNT];
		ITEMTYPE onGround[ITEM_COUNT];
		SCROLLER scrollers[INVENTORYSCREENSCROLLER_COUNT];
		INVENTORYSCREENSCROLLERTYPE currentScroller;
	};

#ifdef __cplusplus
}
#endif

#endif

