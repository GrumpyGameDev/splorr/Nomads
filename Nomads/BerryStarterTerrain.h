#ifndef BERRYSTARTER_TERRAIN_H
#define BERRYSTARTER_TERRAIN_H

#ifdef __cplusplus
extern "C" {
#endif

	typedef struct BerryStarterTerrain BERRYSTARTERTERRAIN;

	int BerryStarterTerrain_getGrowthTimer(BERRYSTARTERTERRAIN* ptr);
	void BerryStarterTerrain_setGrowthTimer(BERRYSTARTERTERRAIN* ptr, int growthTimer);
	void BerryStarterTerrain_changeGrowthTimerBy(BERRYSTARTERTERRAIN* ptr, int delta);


#ifdef __cplusplus
}
#endif

#endif

