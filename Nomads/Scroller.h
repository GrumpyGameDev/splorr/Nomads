#ifndef SCROLLER_H
#define SCROLLER_H

#ifdef __cplusplus
extern "C" {
#endif

	typedef struct Scroller SCROLLER;

	void Scroller_initialize(SCROLLER* ptr, int itemCount, int listSize);
	int Scroller_getItemCount(SCROLLER* ptr);
	int Scroller_getListSize(SCROLLER* ptr);
	int Scroller_getScroll(SCROLLER* ptr);
	int Scroller_getIndex(SCROLLER* ptr);
	int Scroller_getMaximumScroll(SCROLLER* ptr);
	void Scroller_refresh(SCROLLER* ptr);
	void Scroller_setItemCount(SCROLLER* ptr, int itemCount);
	void Scroller_nextIndex(SCROLLER* ptr);
	void Scroller_previousIndex(SCROLLER* ptr);
	int Scroller_getThumbTop(SCROLLER* ptr, int offset, int height);
	int Scroller_getThumbBottom(SCROLLER* ptr, int offset, int height);
	void Scroller_getItemIndices(SCROLLER* ptr, int indexCount, int* indices);
	int Scroller_canScrollUp(SCROLLER* ptr);
	int Scroller_canScrollDown(SCROLLER* ptr);

#ifdef __cplusplus
}
#endif

#endif

