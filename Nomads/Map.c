#include "Map_internals.h"
#include "Utility.h"
#include <assert.h>

MAPCOLUMN* Map_getMapColumn(MAP* ptr, int column)
{
	assert(ptr);
	return &(ptr->columns[Utility_modulo(column,MAP_COLUMNS)]);
}

void Map_doTimers(MAP* ptr)
{
	assert(ptr);
	for(int column=0;column<MAP_COLUMNS;++column)
	{
		MapColumn_doTimers(Map_getMapColumn(ptr,column));
	}
}

MAPCELL* Map_getMapCell(MAP* ptr, int column, int row)
{
	assert(ptr);
	return MapColumn_getMapCell(Map_getMapColumn(ptr, column), row);
}

void Map_findTerrain(MAP* ptr, TERRAIN* terrain, int* x, int *y)
{
	assert(ptr);
	assert(terrain);
	assert(x);
	assert(y);

	*x = -1;
	*y = -1;

	for (int column = 0; column < MAP_COLUMNS; ++column)
	{
		for (int row = 0; row < MAP_ROWS; ++row)
		{
			MAPCELL* cell = Map_getMapCell(ptr, column, row);
			if (MapCell_getTerrain(cell) == terrain)
			{
				*x = column;
				*y = row;
				column = MAP_COLUMNS;
				row = MAP_ROWS;
			}
		}
	}

}
