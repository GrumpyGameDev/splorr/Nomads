#ifndef FLINTROCK_TERRAIN_INTERNALS_H
#define FLINTROCK_TERRAIN_INTERNALS_H

#ifdef __cplusplus
extern "C" {
#endif

#include "FlintrockTerrain.h"
#include "TerrainCommon_internals.h"

	struct FlintrockTerrain {
		TERRAINCOMMON common;
		int flintLeft;
	};

	void FlintrockTerrain_initialize(union Terrain* ptr, union TerrainDescriptor* descriptor);



#ifdef __cplusplus
}
#endif

#endif

