#ifndef WATER_TERRAIN_INTERNALS_H
#define WATER_TERRAIN_INTERNALS_H

#ifdef __cplusplus
extern "C" {
#endif

#include "WaterTerrain.h"
#include "TerrainCommon_internals.h"

	struct WaterTerrain {
		TERRAINCOMMON common;
		int fishLeft;
		int maximumFish;
		int restockTimer;
	};
	void WaterTerrain_initialize(union Terrain* ptr, union TerrainDescriptor* descriptor);



#ifdef __cplusplus
}
#endif

#endif

