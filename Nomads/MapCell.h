#ifndef MAP_CELL_H
#define MAP_CELL_H

#ifdef __cplusplus
extern "C" {
#endif

#include "Creature.h"
#include "Terrain.h"
#include "ItemType.h"

typedef struct MapCell MAPCELL;

int MapCell_canEnter(MAPCELL* ptr, CREATURE* creature);
int MapCell_canInteract(MAPCELL* ptr, CREATURE* creature, VERBTYPE verb);
void MapCell_interact(MAPCELL* ptr, CREATURE* creature, VERBTYPE verb);
void MapCell_doTimers(MAPCELL* ptr);

TERRAIN* MapCell_getTerrain(MAPCELL* ptr);
CREATURE* MapCell_getCreature(MAPCELL* ptr);
INVENTORY* MapCell_getInventory(MAPCELL* ptr);

int MapCell_getInventoryCount(MAPCELL* ptr, ITEMTYPE item);
void MapCell_changeInventoryCountBy(MAPCELL* ptr, ITEMTYPE item, int delta);

#ifdef __cplusplus
}
#endif

#endif

