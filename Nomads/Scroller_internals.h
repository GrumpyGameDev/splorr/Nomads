#ifndef SCROLLER_INTERNALS_H
#define SCROLLER_INTERNALS_H

#ifdef __cplusplus
extern "C" {
#endif

#include "Scroller.h"

	struct Scroller {
		int itemCount;
		int listSize;
		int scroll;
		int index;
	};

#ifdef __cplusplus
}
#endif

#endif

