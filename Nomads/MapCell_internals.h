#ifndef MAP_CELL_INTERNALS_H
#define MAP_CELL_INTERNALS_H

#ifdef __cplusplus
extern "C" {
#endif

#include "MapCell.h"
#include "Creature_internals.h"
#include "Terrain_internals.h"

typedef struct MapCell {
	CREATURE creature;
	INVENTORY inventory;
	TERRAIN terrain;
} MAPCELL;

#ifdef __cplusplus
}
#endif

#endif