#ifndef TAGON_H
#define TAGON_H

#ifdef __cplusplus
extern "C" {
#endif

#include "CreatureCommon.h"
#include "ItemType.h"
#include "StatisticType.h"
#include "Statistic.h"
#include "Inventory.h"
#include "VerbType.h"

typedef enum ExertionType {
	EXERTION_NONE,
	EXERTION_LOW,
	EXERTION_MODERATE,
	EXERTION_HIGH,
	EXERTION_MAXIMUM,

	EXERTION_COUNT,
	EXERTION_FIRST = EXERTION_NONE
} EXERTIONTYPE;

typedef struct Tagon TAGON;

STATISTIC* Tagon_getStatistic(TAGON* ptr,STATISTICTYPE statistic);
void Tagon_changeStatisticBy(TAGON* ptr, STATISTICTYPE statistic, int delta);
int Tagon_getCurrentStatistic(TAGON* ptr, STATISTICTYPE statistic);

INVENTORY* Tagon_getInventory(TAGON* ptr);
int Tagon_hasInventoryCount(TAGON* ptr, ITEMTYPE item);
int Tagon_getInventoryCount(TAGON* ptr, ITEMTYPE item);

void Tagon_postInteract(TAGON* ptr);
void Tagon_changeInventoryCountBy(TAGON* ptr, ITEMTYPE item, int delta);
void Tagon_useVerb(TAGON* ptr, VERBTYPE verb);
void Tagon_setExertion(TAGON * ptr, EXERTIONTYPE exertion);
EXERTIONTYPE Tagon_getExertion(TAGON* ptr);
void Tagon_setVerb(TAGON* ptr, VERBTYPE verb);
VERBTYPE Tagon_getVerb(TAGON* ptr);
int Tagon_canDoVerb(TAGON* ptr, VERBTYPE verb);
int Tagon_canDoCurrentVerb(TAGON* ptr);

#ifdef __cplusplus
}
#endif

#endif

