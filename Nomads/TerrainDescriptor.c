#include "TerrainDescriptor_internals.h"
#include "Terrain.h"
#include "VerbType.h"
#include "TerrainType.h"
#include "Generators.h"
#include "MapCell.h"
#include "ItemDescriptors.h"
#include "TerrainDescriptors.h"
#include "Utility.h"
#include <assert.h>
#include "Terrain_internals.h"
#include "Map_internals.h"
#include "Game.h"
#include "VerbDescriptors.h"
#include <stdio.h>
#include "Verb_internals.h"

void TerrainDescriptor_initializeInstance(TERRAINDESCRIPTOR* descriptor, union Terrain* ptr)
{
	assert(ptr);
	assert(descriptor);
	descriptor->common.initializeFunc(ptr, descriptor);
}

int TerrainDescriptor_canSpawnCreature(TERRAINDESCRIPTOR* ptr, CREATURETYPE creatureType)
{
	assert(ptr);
	assert(creatureType >= CREATURE_FIRST && creatureType < CREATURE_COUNT);
	switch (TerrainDescriptor_getType(ptr))
	{
	case TERRAIN_GRASS:
	case TERRAIN_STUMP:
	case TERRAIN_RUBBLE:
		return 1;
	default:
		return 0;
	}
}

int TerrainDescriptor_canEnter(TERRAINDESCRIPTOR* ptr, CREATURE* creature)
{
	assert(ptr);
	assert(creature);
	switch (TerrainDescriptor_getType(ptr))
	{
	case TERRAIN_GRASS:
	case TERRAIN_STUMP:
	case TERRAIN_RUBBLE:
	case TERRAIN_BERRY_STARTER:
	case TERRAIN_RABBIT_HOLE:
	case TERRAIN_DIRT:
	case TERRAIN_ACORN:
		return 1;
	default:
		return 0;
	}
}

int TerrainDescriptor_canInteract(TERRAINDESCRIPTOR* ptr, CREATURE* creature, VERBTYPE verb)//TODO: refactor to get rid of this
{
	assert(ptr);
	assert(creature);
	switch (TerrainDescriptor_getType(ptr))
	{
	default:
		return 1;
	}
}

////////////////////////////////////////////////////////////////////////////////
//Timers
////////////////////////////////////////////////////////////////////////////////

void TerrainDescriptor_doNothingTimer(TERRAINDESCRIPTOR* ptr, struct MapCell* cell) {}

void TerrainDescriptor_doTimers(TERRAINDESCRIPTOR* ptr, struct MapCell* cell)
{
	assert(ptr);
	assert(cell);
	ptr->common.timerFunc(ptr, cell);
}

////////////////////////////////////////////////////////////////////////////////
//Interactions
////////////////////////////////////////////////////////////////////////////////

typedef void(*TerrainDescriptorInteractFunc)(TERRAINDESCRIPTOR*, TERRAIN*, CREATURE*, VERBTYPE);

static void InvalidVerb()
{
	Game_writeMessage("Doesn't work!", COLOR_SILVER);
}

static void LookVerb_interact(TERRAINDESCRIPTOR* ptr)
{
	assert(ptr);
	char buffer[MESSAGEFEED_COLUMNS + 1];
	sprintf_s(buffer, MESSAGEFEED_COLUMNS, "You see %s", TerrainDescriptor_getName(ptr));
	Game_writeMessage(buffer, COLOR_SILVER);
}

static void DefaultInteraction(TERRAINDESCRIPTOR* ptr, TERRAIN* terrain, CREATURE* creature, VERBTYPE verb)
{
	assert(ptr);
	assert(terrain);
	assert(creature);
	assert(verb >= VERB_FIRST && verb < VERB_COUNT);
	if (Creature_getType(creature) != CREATURE_TAGON)
	{
		return;
	}
	else if (verb == VERB_LOOK)
	{
		LookVerb_interact(ptr);
	}
	else
	{
		InvalidVerb();
	}
}

static void StumpInteraction(TERRAINDESCRIPTOR* ptr, TERRAIN* terrain, CREATURE* creature, VERBTYPE verb)
{
	assert(ptr);
	assert(terrain);
	assert(creature);
	assert(verb >= VERB_FIRST && verb < VERB_COUNT);
	if (Creature_getType(creature) != CREATURE_TAGON)
	{
		return;
	}
	TAGON* tagon = Creature_getTagon(creature);
	if (verb == VERB_USE_STONE_SHOVEL || verb == VERB_USE_WOOD_SHOVEL)
	{
		ShovelVerb_do(tagon, terrain, verb);
	}
	else
	{
		DefaultInteraction(ptr, terrain, creature, verb);
	}
}

static void RubbleInteraction(TERRAINDESCRIPTOR* ptr, TERRAIN* terrain, CREATURE* creature, VERBTYPE verb)
{
	assert(ptr);
	assert(terrain);
	assert(creature);
	assert(verb >= VERB_FIRST && verb < VERB_COUNT);
	if (Creature_getType(creature) != CREATURE_TAGON)
	{
		return;
	}
	TAGON* tagon = Creature_getTagon(creature);
	if (verb == VERB_USE_STONE_SHOVEL || verb == VERB_USE_WOOD_SHOVEL)
	{
		ShovelVerb_do(tagon, terrain, verb);
	}
	else
	{
		DefaultInteraction(ptr, terrain, creature, verb);
	}
}


static void PlantBerrySeed(TERRAIN* terrain, CREATURE* creature, VERBTYPE verb)
{
	TAGON* tagon = Creature_getTagon(creature);
	VERBDESCRIPTOR* verbDescriptor = VerbDescriptors_getDescriptor(verb);
	char buffer[MESSAGEFEED_COLUMNS + 1] = { 0 };

	Tagon_changeStatisticBy(tagon, STATISTIC_ENERGY, -VerbDescriptor_getEnergy(verbDescriptor));
	Tagon_changeInventoryCountBy(tagon, VerbDescriptor_getItem(verbDescriptor), -1);

	sprintf_s(buffer, MESSAGEFEED_COLUMNS, "-1 %s (%d)", ItemDescriptor_getName(ItemDescriptors_getDescriptor(VerbDescriptor_getItem(verbDescriptor))), Tagon_getInventoryCount(tagon, VerbDescriptor_getItem(verbDescriptor)));
	Game_writeMessage(buffer, COLOR_SILVER);

	TerrainDescriptor_initializeInstance(TerrainDescriptors_getDescriptor(TERRAIN_BERRY_STARTER), terrain);
}

static void GrassInteraction(TERRAINDESCRIPTOR* ptr, TERRAIN* terrain, CREATURE* creature, VERBTYPE verb) 
{
	assert(ptr);
	assert(terrain);
	assert(creature);
	if (Creature_getType(creature) != CREATURE_TAGON)
	{
		return;
	}
	else if (verb == VERB_PLACE_GRAVEL)
	{
		TAGON* tagon = Creature_getTagon(creature);
		PlaceGravelVerb_do(tagon, terrain);
	}
	else if (verb == VERB_PLANT_BERRY)
	{
		PlantBerrySeed(terrain, creature, verb);
	}
	else
	{
		DefaultInteraction(ptr, terrain, creature,verb);
	}
}

static void DirtInteraction(TERRAINDESCRIPTOR* ptr, TERRAIN* terrain, CREATURE* creature, VERBTYPE verb)
{
	assert(ptr);
	assert(terrain);
	assert(creature);
	if (Creature_getType(creature) != CREATURE_TAGON)
	{
		return;
	}
	TAGON* tagon = Creature_getTagon(creature);
	if (verb == VERB_PLACE_GRAVEL)
	{
		PlaceGravelVerb_do(tagon, terrain);
	}
	else if (verb == VERB_PLANT_BERRY)
	{
		PlantBerrySeed(terrain, creature, verb);
	}
	else
	{
		DefaultInteraction(ptr, terrain, creature, verb);
	}
}

static void BushInteraction(TERRAINDESCRIPTOR* ptr, TERRAIN* terrain, CREATURE* creature, VERBTYPE verb)
{
	assert(ptr);
	assert(terrain);
	assert(creature);
	char buffer[MESSAGEFEED_COLUMNS + 1];
	if (Creature_getType(creature) != CREATURE_TAGON)
	{
		return;
	}
	else if (verb == VERB_USE_WOOD_AXE || verb == VERB_USE_STONE_AXE)
	{
		TAGON* tagon = Creature_getTagon(creature);
		//chop down bush
		//requires energy of 1
		Tagon_changeStatisticBy(tagon, STATISTIC_ENERGY, -VerbDescriptors_getEnergy(verb));
		//reduces item (some sort of axe) by 1
		Tagon_changeInventoryCountBy(tagon, VerbDescriptors_getItem(verb), -1);
		//changes bush into dead bush
		//change dead bush into dirt
		if (Terrain_getType(terrain) == TERRAIN_BUSH)
		{
			TerrainDescriptor_initializeInstance(TerrainDescriptors_getDescriptor(TERRAIN_DEAD_BUSH), terrain);
		}
		else
		{
			TerrainDescriptor_initializeInstance(TerrainDescriptors_getDescriptor(TERRAIN_DIRT), terrain);
		}
	}
	else if (verb == VERB_TALK)
	{

	}
	else if(verb== VERB_PICK_BERRY)
	{
		TAGON* tagon = Creature_getTagon(creature);

		int picking = 1;

		while (picking > 0)
		{
			picking--;
			int column;
			int row;
			Map_findTerrain(World_getMap(Game_getWorld()), terrain, &column, &row);
			assert(column != -1 && row != -1);

			MAPCELL* cell = Map_getMapCell(World_getMap(Game_getWorld()), column, row);
			ITEMTYPE item = Inventory_getFirstItem(MapCell_getInventory(cell));
			if (item == ITEM_NONE)
			{
				picking = 0;
			}
			else
			{
				Inventory_changeCountBy(MapCell_getInventory(cell), item, -1);
				Inventory_changeCountBy(Tagon_getInventory(tagon), item, 1);

				sprintf_s(buffer, MESSAGEFEED_COLUMNS, "+1 %s (%d)", ItemDescriptor_getName(ItemDescriptors_getDescriptor(item)), Tagon_getInventoryCount(tagon, item));
				Game_writeMessage(buffer, COLOR_SILVER);
			}
		}
	}
	else
	{
		DefaultInteraction(ptr, terrain, creature, verb);
	}
}

static void TreeInteraction(TERRAINDESCRIPTOR* ptr, TERRAIN* terrain, CREATURE* creature, VERBTYPE verb)
{
	assert(ptr);
	assert(terrain);
	assert(creature);
	char buffer[MESSAGEFEED_COLUMNS + 1];
	if(Creature_getType(creature)!=CREATURE_TAGON)
	{
		return;
	}
	else if (verb == VERB_TALK)
	{
		sprintf_s(buffer, sizeof(buffer), "The tree seems unsympathetic.");
		Game_writeMessage(buffer, COLOR_SILVER);
	}
	else if (verb == VERB_LOOK)
	{
		TREETERRAIN* tree = Terrain_getTree(terrain);
		sprintf_s(buffer, sizeof(buffer), "%s (%d logs)", TerrainDescriptor_getName(ptr) , TreeTerrain_getWoodLeft(tree));
		Game_writeMessage(buffer, COLOR_SILVER);
	}
	else if (
		(verb == VERB_TAKE_LOG) || 
		(verb == VERB_USE_WOOD_AXE) || 
		(verb == VERB_USE_STONE_AXE))
	{
		TAGON* tagon = Creature_getTagon(creature);

		if (verb == VERB_TAKE_LOG)
		{
			if (Statistic_getCurrent(Tagon_getStatistic(tagon, STATISTIC_ENERGY)) < 1)
			{
				Game_writeMessage("Not enough energy!", COLOR_SILVER);
				return;
			}
		}

		//determine chopping ability of item
		int chopping = 
			(verb == VERB_USE_STONE_AXE) ? (2) :
			(verb == VERB_USE_WOOD_AXE) ? (2) :
			(1);

		//determine wood left in tree
		TREETERRAIN* tree = Terrain_getTree(terrain);
		chopping = Utility_minimum(chopping, TreeTerrain_getWoodLeft(tree));

		//add wood to tagon inventory
		Tagon_changeInventoryCountBy(tagon, ITEM_LOG, chopping);

		VERBDESCRIPTOR* verbDescriptor = VerbDescriptors_getDescriptor(verb);
		ITEMTYPE item = VerbDescriptor_getItem(verbDescriptor);
		if (item != ITEM_NONE)
		{
			Tagon_changeInventoryCountBy(tagon, item, -1);
		}

		//remove 1 energy
		Tagon_changeStatisticBy(tagon, STATISTIC_ENERGY, -VerbDescriptors_getEnergy(verb));

		sprintf_s(buffer, sizeof(buffer), "Log +%d (%d)", chopping, Tagon_getInventoryCount(tagon,ITEM_LOG));
		Game_writeMessage(buffer, COLOR_SILVER);

		//remove wood from tree
		TreeTerrain_changeWoodLeftBy(tree, -chopping);

		//if tree is out of wood, make it a stump
		if (!TreeTerrain_hasWoodLeft(tree))
		{
			TerrainDescriptor_initializeInstance(TerrainDescriptors_getDescriptor(TERRAIN_STUMP), terrain);
		}
	}
	else
	{
		DefaultInteraction(ptr, terrain, creature, verb);
	}
}

static void RockInteraction(TERRAINDESCRIPTOR* ptr, TERRAIN* terrain, CREATURE* creature, VERBTYPE verb)
{
	assert(ptr);
	assert(terrain);
	assert(creature);
	char buffer[MESSAGEFEED_COLUMNS + 1];
	if (Creature_getType(creature) != CREATURE_TAGON)
	{
		return;
	}
	else if (verb == VERB_TALK)
	{
		sprintf_s(buffer, sizeof(buffer), "The rock is not impressed.");
		Game_writeMessage(buffer, COLOR_SILVER);
	}
	else if (verb == VERB_LOOK)
	{
		ROCKTERRAIN* rock = Terrain_getRock(terrain);
		sprintf_s(buffer, sizeof(buffer), "%s (%d stone)", TerrainDescriptor_getName(ptr), RockTerrain_getStoneLeft(rock));
		Game_writeMessage(buffer, COLOR_SILVER);
	}
	else if(
		(verb==VERB_USE_WOOD_PICK) ||
		(verb == VERB_USE_STONE_PICK))
	{
		TAGON* tagon = Creature_getTagon(creature);

		//determine chopping ability of item
		int mining = 
			(verb == VERB_USE_STONE_PICK) ? (1) :
			(verb == VERB_USE_WOOD_PICK) ? (1) :
			(0);

		//determine wood left in tree
		ROCKTERRAIN* rock = Terrain_getRock(terrain);
		mining = Utility_minimum(mining, RockTerrain_getStoneLeft(rock));

		//add wood to tagon inventory
		Tagon_changeInventoryCountBy(tagon, ITEM_STONE, mining);

		//deplete tool if needed
		VERBDESCRIPTOR* verbDescriptor = VerbDescriptors_getDescriptor(verb);
		ITEMTYPE item = VerbDescriptor_getItem(verbDescriptor);
		if (item != ITEM_NONE)
		{
			Tagon_changeInventoryCountBy(tagon, item, -1);
		}

		//remove 1 energy
		Tagon_changeStatisticBy(tagon, STATISTIC_ENERGY, -VerbDescriptors_getEnergy(verb));

		sprintf_s(buffer, sizeof(buffer), "Stone +%d (%d)", mining, Tagon_getInventoryCount(tagon, ITEM_STONE));
		Game_writeMessage(buffer, COLOR_SILVER);


		//remove wood from tree
		RockTerrain_changeStoneLeftBy(rock, -mining);

		//if tree is out of wood, make it a stump
		if (!RockTerrain_hasStoneLeft(rock))
		{
			TerrainDescriptor_initializeInstance(TerrainDescriptors_getDescriptor(TERRAIN_RUBBLE), terrain);
		}
	}
	else
	{
		DefaultInteraction(ptr, terrain, creature, verb);
	}
}

static void FlintrockInteraction(TERRAINDESCRIPTOR* ptr, TERRAIN* terrain, CREATURE* creature, VERBTYPE verb)
{
	assert(ptr);
	assert(terrain);
	assert(creature);
	char buffer[MESSAGEFEED_COLUMNS + 1];
	if (Creature_getType(creature) != CREATURE_TAGON)
	{
		return;
	}
	else if (verb == VERB_TALK)
	{
		sprintf_s(buffer, sizeof(buffer), "The flintrock listens intently.");
		Game_writeMessage(buffer, COLOR_SILVER);
	}
	else if (verb == VERB_LOOK)
	{
		FLINTROCKTERRAIN* rock = Terrain_getFlintrock(terrain);
		sprintf_s(buffer, sizeof(buffer), "%s (%d flint)", TerrainDescriptor_getName(ptr), FlintrockTerrain_getFlintLeft(rock));
		Game_writeMessage(buffer, COLOR_SILVER);
	}
	else if (
		(verb == VERB_USE_STONE_PICK))
	{
		TAGON* tagon = Creature_getTagon(creature);

		//determine chopping ability of item
		int mining =
			(verb == VERB_USE_STONE_PICK) ? (1) :
			(0);

		//determine wood left in tree
		FLINTROCKTERRAIN* rock = Terrain_getFlintrock(terrain);
		mining = Utility_minimum(mining, FlintrockTerrain_getFlintLeft(rock));

		//add wood to tagon inventory
		Tagon_changeInventoryCountBy(tagon, ITEM_FLINT, mining);

		//deplete tool if needed
		VERBDESCRIPTOR* verbDescriptor = VerbDescriptors_getDescriptor(verb);
		ITEMTYPE item = VerbDescriptor_getItem(verbDescriptor);
		if (item != ITEM_NONE)
		{
			Tagon_changeInventoryCountBy(tagon, item, -1);
		}

		//remove 1 energy
		Tagon_changeStatisticBy(tagon, STATISTIC_ENERGY, -VerbDescriptors_getEnergy(verb));

		sprintf_s(buffer, sizeof(buffer), "Flint +%d (%d)", mining, Tagon_getInventoryCount(tagon, ITEM_FLINT));
		Game_writeMessage(buffer, COLOR_SILVER);


		//remove wood from tree
		FlintrockTerrain_changeFlintLeftBy(rock, -mining);

		//if tree is out of wood, make it a stump
		if (!FlintrockTerrain_hasFlintLeft(rock))
		{
			TerrainDescriptor_initializeInstance(TerrainDescriptors_getDescriptor(TERRAIN_RUBBLE), terrain);
		}
	}
	else
	{
		DefaultInteraction(ptr, terrain, creature, verb);
	}
}

static TerrainDescriptorInteractFunc terrainDescriptorInteractFuncs[TERRAIN_COUNT]=
{
	GrassInteraction,//grass
	TreeInteraction,//tree
	RockInteraction,//rock
	FlintrockInteraction,//flint rock
	BushInteraction,//bush
	DefaultInteraction,//rabbit hole
	DefaultInteraction,//water
	StumpInteraction,//stump
	RubbleInteraction,//rubble
	BushInteraction,//dead bush
	DefaultInteraction,//bush starter
	DirtInteraction,//dirt
	DefaultInteraction//acorn
};

void TerrainDescriptor_interact(TERRAINDESCRIPTOR* ptr, union Terrain* terrain, CREATURE* creature, VERBTYPE verb)
{
	assert(ptr);
	assert(terrain);
	assert(creature);
	terrainDescriptorInteractFuncs[TerrainDescriptor_getType(ptr)](ptr,terrain,creature,verb);
}

TERRAINTYPE TerrainDescriptor_getType(TERRAINDESCRIPTOR* ptr)
{
	assert(ptr);
	return DescriptorCommon_getTerrainType(TerrainDescriptor_getCommon(ptr));
}

PATTERNTYPE TerrainDescriptor_getPattern(TERRAINDESCRIPTOR* ptr)
{
	assert(ptr);
	return DescriptorCommon_getPattern(TerrainDescriptor_getCommon(ptr));
}

COLORTYPE TerrainDescriptor_getForeground(TERRAINDESCRIPTOR* ptr)
{
	assert(ptr);
	return DescriptorCommon_getForeground(TerrainDescriptor_getCommon(ptr));
}

COLORTYPE TerrainDescriptor_getBackground(TERRAINDESCRIPTOR* ptr)
{
	assert(ptr);
	return DescriptorCommon_getBackground(TerrainDescriptor_getCommon(ptr));
}

DESCRIPTORCOMMON* TerrainDescriptor_getCommon(TERRAINDESCRIPTOR* ptr)
{
	assert(ptr);
	return &(ptr->common.common);
}

const char* TerrainDescriptor_getName(TERRAINDESCRIPTOR* ptr)
{
	assert(ptr);
	return ptr->common.name;
}