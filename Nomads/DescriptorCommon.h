#ifndef DESCRIPTOR_COMMON_H
#define DESCRIPTOR_COMMON_H

#ifdef __cplusplus
extern "C" {
#endif

#include "PatternType.h"
#include "ColorType.h"
#include "TerrainType.h"
#include "ItemType.h"
#include "CreatureType.h"
#include "VerbType.h"

typedef struct DescriptorCommon DESCRIPTORCOMMON;

TERRAINTYPE DescriptorCommon_getTerrainType(DESCRIPTORCOMMON* ptr);
CREATURETYPE DescriptorCommon_getCreatureType(DESCRIPTORCOMMON* ptr);
ITEMTYPE DescriptorCommon_getItemType(DESCRIPTORCOMMON* ptr);
VERBTYPE DescriptorCommon_getVerbType(DESCRIPTORCOMMON* ptr);
PATTERNTYPE DescriptorCommon_getPattern(DESCRIPTORCOMMON* ptr);
COLORTYPE DescriptorCommon_getForeground(DESCRIPTORCOMMON* ptr);
COLORTYPE DescriptorCommon_getBackground(DESCRIPTORCOMMON* ptr);

#ifdef __cplusplus
}
#endif

#endif
