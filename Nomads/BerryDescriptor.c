#include "BerryDescriptor_internals.h"
#include <assert.h>
#include "Tagon.h"
#include "ItemDescriptor.h"

BERRYTYPE BerryDescriptor_getBerry(BERRYDESCRIPTOR* ptr)
{
	assert(ptr);
	return ptr->berryType;
}

ITEMDESCRIPTORCOMMON* BerryDescriptor_getItemCommon(BERRYDESCRIPTOR* ptr)
{
	assert(ptr);
	return &(ptr->common);
}

PATTERNTYPE BerryDescriptor_getPattern(BERRYDESCRIPTOR* ptr)
{
	assert(ptr);
	return ItemDescriptorCommon_getPattern(BerryDescriptor_getItemCommon(ptr));
}

COLORTYPE BerryDescriptor_getForeground(BERRYDESCRIPTOR* ptr)
{
	assert(ptr);
	return ItemDescriptorCommon_getForeground(BerryDescriptor_getItemCommon(ptr));
}

COLORTYPE BerryDescriptor_getBackground(BERRYDESCRIPTOR* ptr)
{
	assert(ptr);
	return ItemDescriptorCommon_getBackground(BerryDescriptor_getItemCommon(ptr));
}

int BerryDescriptor_getEnergy(BERRYDESCRIPTOR* ptr)
{
	assert(ptr);
	return ptr->energy;
}

