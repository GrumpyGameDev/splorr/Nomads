#ifndef VIEW_INTERNALS_H
#define VIEW_INTERNALS_H

#ifdef __cplusplus
extern "C" {
#endif

#include "ViewCommon_internals.h"
#include "MainMenu_internals.h"
#include "ConfirmQuit_internals.h"
#include "MapScreen_internals.h"
#include "CraftScreen_internals.h"
#include "InventoryScreen_internals.h"
#include "VerbToolbar_internals.h"
#include "View.h"

union View {
	VIEWCOMMON viewCommon;
	MAINMENU mainMenu;
	CONFIRMQUIT confirmQuit;
	MAPSCREEN mapScreen;
	CRAFTSCREEN craftScreen;
	INVENTORYSCREEN inventoryScreen;
	VERBTOOLBAR verbToolbar;
};

VIEWCOMMON* View_getCommon(VIEW* ptr);

#ifdef __cplusplus
}
#endif

#endif

