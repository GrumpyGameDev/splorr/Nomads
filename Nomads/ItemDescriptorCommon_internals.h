#ifndef ITEM_DESCRIPTOR_COMMON_INTERNALS_H
#define ITEM_DESCRIPTOR_COMMON_INTERNALS_H

#ifdef __cplusplus
extern "C" {
#endif

#include "ItemDescriptorCommon.h"
#include "DescriptorCommon_internals.h"

union ItemDescriptor;
struct Tagon;

struct ItemDescriptorCommon {
	DESCRIPTORCOMMON common;
	const char* name;
};

DESCRIPTORCOMMON* ItemDescriptorCommon_getCommon(ITEMDESCRIPTORCOMMON* ptr);

#ifdef __cplusplus
}
#endif

#endif

