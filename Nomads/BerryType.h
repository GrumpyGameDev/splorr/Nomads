#ifndef BERRY_TYPE_H
#define BERRY_TYPE_H

#ifdef __cplusplus
extern "C" {
#endif

typedef enum BerryType {
	BERRY_CARNELIAN,
	BERRY_COPPER,
	BERRY_GOLD,
	BERRY_JADE,
	BERRY_TURQUOISE,
	BERRY_RUBY,
	BERRY_AMETHYST,
	BERRY_SILVER,

	BERRY_COUNT,
	BERRY_FIRST = BERRY_CARNELIAN
} BERRYTYPE;

#ifdef __cplusplus
}
#endif

#endif

