#ifndef CREATURE_TYPE_H
#define CREATURE_TYPE_H

#ifdef __cplusplus
extern "C" {
#endif

typedef enum CreatureType{
	CREATURE_TAGON,
	CREATURE_COUNT,

	CREATURE_FIRST = CREATURE_TAGON,
	CREATURE_NONE=CREATURE_COUNT
} CREATURETYPE;

#ifdef __cplusplus
}
#endif

#endif
