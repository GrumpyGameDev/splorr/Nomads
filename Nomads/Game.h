#ifndef GAME_H
#define GAME_H

#ifdef __cplusplus
extern "C" {
#endif

#include "World.h"
#include "DirectionType.h"

void Game_initialize(void);

WORLD* Game_getWorld(void);
void Game_moveTagon(DIRECTIONTYPE direction);
INVENTORY* Game_getGroundInventory(void);
void Game_doTimers(void);
TAGON* Game_getTagon(void);	
void Game_tagonInteract(void);
void Game_tagonUseVerb(void);
void Game_writeMessage(const char* message, COLORTYPE color);
MAPCELL* Game_getTagonMapCell(void);



#ifdef __cplusplus
}
#endif

#endif

