#ifndef BERRY_DESCRIPTOR_H
#define BERRY_DESCRIPTOR_H

#ifdef __cplusplus
extern "C" {
#endif

#include "BerryType.h"
#include "PatternType.h"
#include "ColorType.h"

typedef struct BerryDescriptor BERRYDESCRIPTOR;

BERRYTYPE BerryDescriptor_getBerry(BERRYDESCRIPTOR* ptr);
PATTERNTYPE BerryDescriptor_getPattern(BERRYDESCRIPTOR* ptr);
COLORTYPE BerryDescriptor_getForeground(BERRYDESCRIPTOR* ptr);
COLORTYPE BerryDescriptor_getBackground(BERRYDESCRIPTOR* ptr);



#ifdef __cplusplus
}
#endif

#endif

