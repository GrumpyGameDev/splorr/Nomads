#ifndef COLOR_INTERNALS_H
#define COLOR_INTERNALS_H

#ifdef __cplusplus
extern "C" {
#endif

#include "Color.h"

	struct Color {
		unsigned char red;
		unsigned char green;
		unsigned char blue;
		unsigned char alpha;
	};

#ifdef __cplusplus
}
#endif

#endif
