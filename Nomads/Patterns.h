#ifndef PATTERNS_H
#define PATTERNS_H

#ifdef __cplusplus
extern "C" {
#endif

#include "PatternType.h"
#include "Pattern.h"

PATTERN* Patterns_getPattern(PATTERNTYPE pattern);

#ifdef __cplusplus
}
#endif

#endif

