#ifndef DIRT_TERRAIN_H
#define DIRT_TERRAIN_H

#ifdef __cplusplus
extern "C" {
#endif

	typedef struct DirtTerrain DIRTTERRAIN;

	int DirtTerrain_getGrassTimer(DIRTTERRAIN* ptr);
	void DirtTerrain_setGrassTimer(DIRTTERRAIN* ptr, int grassTimer);
	void DirtTerrain_changeGrassTimerBy(DIRTTERRAIN* ptr, int delta);


#ifdef __cplusplus
}
#endif

#endif

