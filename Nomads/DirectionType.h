#ifndef DIRECTION_TYPE_H
#define DIRECTION_TYPE_H

#ifdef __cplusplus
extern "C" {
#endif

typedef enum DirectionType{
	DIRECTION_NORTH,
	DIRECTION_EAST,
	DIRECTION_SOUTH,
	DIRECTION_WEST,
	DIRECTION_COUNT,
	DIRECTION_FIRST = DIRECTION_NORTH
} DIRECTIONTYPE;

#ifdef __cplusplus
}
#endif

#endif

