#ifndef AVATAR_H
#define AVATAR_H

#ifdef __cplusplus
extern "C" {
#endif

#include "DirectionType.h"

typedef struct Avatar AVATAR;

void Avatar_initialize(AVATAR* ptr, int x, int y,DIRECTIONTYPE facing);
void Avatar_move(AVATAR* ptr, DIRECTIONTYPE direction);
void Avatar_setFacing(AVATAR* ptr, DIRECTIONTYPE direction);
void Avatar_doTimers(AVATAR* ptr);
int Avatar_getX(AVATAR* ptr);
int Avatar_getY(AVATAR* ptr);
DIRECTIONTYPE Avatar_getFacing(AVATAR* ptr);

#ifdef __cplusplus
}
#endif

#endif

