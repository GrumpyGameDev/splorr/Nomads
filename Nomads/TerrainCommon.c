#include "TerrainCommon_internals.h"
#include <assert.h>

TERRAINTYPE TerrainCommon_getType(TERRAINCOMMON* ptr)
{
	assert(ptr);
	return ptr->type;
}

void TerrainCommon_setType(TERRAINCOMMON* ptr, TERRAINTYPE terrain)
{
	assert(ptr);
	ptr->type = terrain;
}
