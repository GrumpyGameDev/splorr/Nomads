#include "MessageFeed.h"
#include "PixelPlotter_internals.h"
#include "View_internals.h"
#include <string.h>
#include "TextPlotter.h"
#include "World.h"
#include "Game.h"
#include <assert.h>

#define MESSAGEFEED_WIDTH (SCALE_WIDTH(SCALE_SMALL))
#define MESSAGEFEED_HEIGHT ((MESSAGEFEED_ROWS)*(PATTERN_WIDTH))

void MessageFeed_initialize(union View* ptr)
{
	assert(ptr);
	PixelPlotterState_initialize(View_getPixelPlotterState(ptr),
		SCALE_CENTER_X(SCALE_SMALL,MESSAGEFEED_WIDTH),
		SCALE_HEIGHT(SCALE_SMALL)-MESSAGEFEED_HEIGHT-(PATTERN_HEIGHT * 3),
		SCALE_SMALL,
		SCALE_SMALL,
		1,
		MESSAGEFEED_WIDTH,
		MESSAGEFEED_HEIGHT);


	ptr->viewCommon.viewType = VIEW_MESSAGE_FEED;
	ptr->viewCommon.enabled = 1;
}

void MessageFeed_draw(union View* ptr)
{
	assert(ptr);
	PIXELPLOTTERSTATE old;
	PixelPlotter_getState(&old);
	PixelPlotter_setState(View_getPixelPlotterState(ptr));

	for(int row=0;row<MESSAGEFEED_ROWS;++row)
	{
		TextPlotter_plotText(0,row*PATTERN_HEIGHT,World_getMessageFeed(Game_getWorld())->lines[row].text,World_getMessageFeed(Game_getWorld())->lines[row].color,COLOR_TRANSPARENT);
	}

	PixelPlotter_setState(&old);
}

int MessageFeed_process(union View* ptr,VIEWCOMMANDTYPE command)
{
	assert(ptr);
	return 0;
}

void MessageFeed_writeMessage(MESSAGEFEED* ptr, const char* message, COLORTYPE color)
{
	assert(ptr);
	assert(message);
	assert(color>=COLOR_FIRST && color<COLOR_COUNT);
	for(int row=0;row<MESSAGEFEED_ROWS-1;++row)
	{
		ptr->lines[row]=ptr->lines[row+1];
	}
	ptr->lines[MESSAGEFEED_ROWS-1].color=color;
	strncpy_s(ptr->lines[MESSAGEFEED_ROWS-1].text, MESSAGEFEED_COLUMNS,message,MESSAGEFEED_COLUMNS);
}


