#ifndef CREATURE_COMMON_INTERNALS_H
#define CREATURE_COMMON_INTERNALS_H

#ifdef __cplusplus
extern "C" {
#endif

#include "CreatureCommon.h"

struct CreatureCommon {
	CREATURETYPE type;
};

#ifdef __cplusplus
}
#endif

#endif

