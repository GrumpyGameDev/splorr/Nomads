#include "ItemDescriptor_internals.h"
#include <assert.h>
#include "Game.h"

void ItemDescriptor_doNothingUse(union ItemDescriptor* descriptor, struct Tagon* tagon, VERBTYPE verb)
{
	assert(descriptor);
	assert(tagon);
	Game_writeMessage("You can't use that.", COLOR_SILVER);
}

ITEMDESCRIPTORCOMMON* ItemDescriptor_getItemCommon(ITEMDESCRIPTOR* ptr)
{
	assert(ptr);
	return &(ptr->itemCommon);
}

PATTERNTYPE ItemDescriptor_getPattern(ITEMDESCRIPTOR* ptr)
{
	assert(ptr);
	return ItemDescriptorCommon_getPattern(ItemDescriptor_getItemCommon(ptr));
}

COLORTYPE ItemDescriptor_getForeground(ITEMDESCRIPTOR* ptr)
{
	assert(ptr);
	return ItemDescriptorCommon_getForeground(ItemDescriptor_getItemCommon(ptr));
}

COLORTYPE ItemDescriptor_getBackground(ITEMDESCRIPTOR* ptr)
{
	assert(ptr);
	return ItemDescriptorCommon_getBackground(ItemDescriptor_getItemCommon(ptr));
}

ITEMTYPE ItemDescriptor_getType(ITEMDESCRIPTOR* ptr)
{
	assert(ptr);
	return ItemDescriptorCommon_getType(ItemDescriptor_getItemCommon(ptr));
}

BERRYDESCRIPTOR* ItemDescriptor_getBerry(ITEMDESCRIPTOR* ptr)
{
	assert(ptr);
	return &(ptr->berry);
}

const char* ItemDescriptor_getName(ITEMDESCRIPTOR* ptr)
{
	return ItemDescriptorCommon_getName(ItemDescriptor_getItemCommon(ptr));
}
