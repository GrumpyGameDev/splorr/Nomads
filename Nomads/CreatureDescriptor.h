#ifndef CREATURE_DESCRIPTOR_H
#define CREATURE_DESCRIPTOR_H

#ifdef __cplusplus
extern "C" {
#endif

#include "DescriptorCommon.h"
#include "CreatureType.h"
#include "VerbType.h"

typedef union CreatureDescriptor CREATUREDESCRIPTOR;

union Creature;

void CreatureDescriptor_initializeInstance(CREATUREDESCRIPTOR* descriptor, union Creature* ptr);
int CreatureDescriptor_canDoVerb(CREATUREDESCRIPTOR* descriptor, union Creature* ptr, VERBTYPE verb);

CREATURETYPE CreatureDescriptor_getType(CREATUREDESCRIPTOR* ptr);
PATTERNTYPE CreatureDescriptor_getPattern(CREATUREDESCRIPTOR* ptr);
COLORTYPE CreatureDescriptor_getForeground(CREATUREDESCRIPTOR* ptr);
COLORTYPE CreatureDescriptor_getBackground(CREATUREDESCRIPTOR* ptr);

#ifdef __cplusplus
}
#endif

#endif
