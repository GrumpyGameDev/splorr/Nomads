#include "Recipes.h"
#include "Recipe_internals.h"
#include <assert.h>

static RECIPE recipes[RECIPE_COUNT] = {0};

typedef void(*RecipeInitialzeFunc)(RECIPE*);

static void woodAxeInitializeFunc(RECIPE* ptr)
{
	assert(ptr);
	Inventory_setCount(Recipe_getInput(ptr), ITEM_LOG, 5);

	Inventory_setCount(Recipe_getOutput(ptr), ITEM_WOOD_AXE, 25);
}

static void stoneAxeInitializeFunc(RECIPE* ptr)
{
	assert(ptr);
	Inventory_setCount(Recipe_getInput(ptr), ITEM_LOG, 2);
	Inventory_setCount(Recipe_getInput(ptr), ITEM_STONE, 3);

	Inventory_setCount(Recipe_getOutput(ptr), ITEM_STONE_AXE, 50);
}

static void woodPickInitializeFunc(RECIPE* ptr)
{
	assert(ptr);
	Inventory_setCount(Recipe_getInput(ptr), ITEM_LOG, 5);

	Inventory_setCount(Recipe_getOutput(ptr), ITEM_WOOD_PICK, 25);
}

static void stonePickInitializeFunc(RECIPE* ptr)
{
	assert(ptr);
	Inventory_setCount(Recipe_getInput(ptr), ITEM_LOG, 2);
	Inventory_setCount(Recipe_getInput(ptr), ITEM_STONE, 3);

	Inventory_setCount(Recipe_getOutput(ptr), ITEM_STONE_PICK, 50);
}

static void woodShovelInitializeFunc(RECIPE* ptr)
{
	assert(ptr);
	Inventory_setCount(Recipe_getInput(ptr), ITEM_LOG, 3);

	Inventory_setCount(Recipe_getOutput(ptr), ITEM_WOOD_SHOVEL, 25);
}

static void stoneShovelInitializeFunc(RECIPE* ptr)
{
	assert(ptr);
	Inventory_setCount(Recipe_getInput(ptr), ITEM_LOG, 2);
	Inventory_setCount(Recipe_getInput(ptr), ITEM_STONE, 1);

	Inventory_setCount(Recipe_getOutput(ptr), ITEM_STONE_SHOVEL, 50);
}

static void woodSpearInitializeFunc(RECIPE* ptr)
{
	assert(ptr);
	Inventory_setCount(Recipe_getInput(ptr), ITEM_LOG, 3);

	Inventory_setCount(Recipe_getOutput(ptr), ITEM_WOOD_SPEAR, 25);
}

static void stoneSpearInitializeFunc(RECIPE* ptr)
{
	assert(ptr);
	Inventory_setCount(Recipe_getInput(ptr), ITEM_LOG, 2);
	Inventory_setCount(Recipe_getInput(ptr), ITEM_STONE, 1);

	Inventory_setCount(Recipe_getOutput(ptr), ITEM_STONE_SPEAR, 50);
}

static void carnelianBerrySeedInitializeFunc(RECIPE* ptr)
{
	assert(ptr);
	Inventory_setCount(Recipe_getInput(ptr), ITEM_CARNELIAN_BERRY, 1);

	Inventory_setCount(Recipe_getOutput(ptr), ITEM_BERRY_SEED, 1);
}

static void copperBerrySeedInitializeFunc(RECIPE* ptr)
{
	assert(ptr);
	Inventory_setCount(Recipe_getInput(ptr), ITEM_COPPER_BERRY, 1);

	Inventory_setCount(Recipe_getOutput(ptr), ITEM_BERRY_SEED, 1);
}

static void goldBerrySeedInitializeFunc(RECIPE* ptr)
{
	assert(ptr);
	Inventory_setCount(Recipe_getInput(ptr), ITEM_GOLD_BERRY, 1);

	Inventory_setCount(Recipe_getOutput(ptr), ITEM_BERRY_SEED, 1);
}

static void jadeBerrySeedInitializeFunc(RECIPE* ptr)
{
	assert(ptr);
	Inventory_setCount(Recipe_getInput(ptr), ITEM_JADE_BERRY, 1);

	Inventory_setCount(Recipe_getOutput(ptr), ITEM_BERRY_SEED, 1);
}

static void turquoiseBerrySeedInitializeFunc(RECIPE* ptr)
{
	assert(ptr);
	Inventory_setCount(Recipe_getInput(ptr), ITEM_TURQUOISE_BERRY, 1);

	Inventory_setCount(Recipe_getOutput(ptr), ITEM_BERRY_SEED, 1);
}

static void rubyBerrySeedInitializeFunc(RECIPE* ptr)
{
	assert(ptr);
	Inventory_setCount(Recipe_getInput(ptr), ITEM_RUBY_BERRY, 1);

	Inventory_setCount(Recipe_getOutput(ptr), ITEM_BERRY_SEED, 1);
}

static void amethystBerrySeedInitializeFunc(RECIPE* ptr)
{
	assert(ptr);
	Inventory_setCount(Recipe_getInput(ptr), ITEM_AMETHYST_BERRY, 1);

	Inventory_setCount(Recipe_getOutput(ptr), ITEM_BERRY_SEED, 1);
}

static void silverBerrySeedInitializeFunc(RECIPE* ptr)
{
	assert(ptr);
	Inventory_setCount(Recipe_getInput(ptr), ITEM_SILVER_BERRY, 1);

	Inventory_setCount(Recipe_getOutput(ptr), ITEM_BERRY_SEED, 1);
}

static RecipeInitialzeFunc recipeInitializeFuncs[RECIPE_COUNT] =
{
	woodAxeInitializeFunc,
	stoneAxeInitializeFunc,

	woodPickInitializeFunc,
	stonePickInitializeFunc,

	woodShovelInitializeFunc,
	stoneShovelInitializeFunc,

	woodSpearInitializeFunc,
	stoneSpearInitializeFunc,

	carnelianBerrySeedInitializeFunc,
	copperBerrySeedInitializeFunc,
	goldBerrySeedInitializeFunc,
	jadeBerrySeedInitializeFunc,
	turquoiseBerrySeedInitializeFunc,
	rubyBerrySeedInitializeFunc,
	amethystBerrySeedInitializeFunc,
	silverBerrySeedInitializeFunc
};

void Recipes_initialize()
{
	for (int index = RECIPE_FIRST; index < RECIPE_COUNT; ++index)
	{
		RECIPE* ptr = &recipes[index];
		recipeInitializeFuncs[index](ptr);
	}
}

RECIPE* Recipes_getRecipe(RECIPETYPE recipeType)
{
	assert(recipeType >= RECIPE_FIRST && recipeType < RECIPE_COUNT);
	return &recipes[recipeType];
}
