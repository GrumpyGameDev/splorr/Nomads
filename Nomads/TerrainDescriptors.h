#ifndef TERRAIN_DESCRIPTORS_H
#define TERRAIN_DESCRIPTORS_H

#ifdef __cplusplus
extern "C" {
#endif

#include "TerrainDescriptor.h"
#include "TerrainType.h"
#include "Terrain.h"

void TerrainDescriptors_initialize(void);
TERRAINDESCRIPTOR* TerrainDescriptors_getDescriptor(TERRAINTYPE terrainType);
void TerrainDescriptors_initializeInstance(TERRAINTYPE terrainType, TERRAIN* terrain);
const char* TerrainDescriptors_getName(TERRAINTYPE terrainType);

#ifdef __cplusplus
}
#endif

#endif

