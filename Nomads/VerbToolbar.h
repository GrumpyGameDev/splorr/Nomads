#ifndef VERB_TOOLBAR_H
#define VERB_TOOLBAR_H

#ifdef __cplusplus
extern "C" {
#endif

#include "ViewCommandType.h"
#include "VerbType.h"

union View;

typedef struct VerbToolbar VERBTOOLBAR;

void VerbToolbar_draw(union View*);
int VerbToolbar_process(union View*, VIEWCOMMANDTYPE);
void VerbToolbar_initialize(union View*);

void VerbToolbar_refresh(VERBTOOLBAR* ptr);
VERBTYPE VerbToolbar_getVerb(VERBTOOLBAR* ptr);


#ifdef __cplusplus
}
#endif

#endif





