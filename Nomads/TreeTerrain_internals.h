#ifndef TREE_TERRAIN_INTERNALS_H
#define TREE_TERRAIN_INTERNALS_H

#ifdef __cplusplus
extern "C" {
#endif

#include "TreeTerrain.h"
#include "TerrainCommon_internals.h"

struct TreeTerrain {
	TERRAINCOMMON common;
	int woodLeft;
	int acornTimer;
};

union TerrainDescriptor;
union Terrain;

void TreeTerrain_doTimer(union TerrainDescriptor*, struct MapCell*);
void TreeTerrain_initialize(union Terrain* ptr, union TerrainDescriptor* descriptor);


#ifdef __cplusplus
}
#endif

#endif

