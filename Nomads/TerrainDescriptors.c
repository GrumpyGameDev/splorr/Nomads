#include "TerrainDescriptors.h"
#include "TerrainDescriptor_internals.h"
#include <string.h>
#include <assert.h>
#include "Terrain_internals.h"

static TERRAINDESCRIPTOR terrainDescriptors[TERRAIN_COUNT] = { 0 };

typedef void(*TerrainDescriptorInitializFunc)(TERRAINDESCRIPTOR*);

static void GrassTerrainDescriptor_initialize(TERRAINDESCRIPTOR* ptr)
{
	assert(ptr);
	ptr->common.common.type.terrainType = TERRAIN_GRASS;
	ptr->common.common.pattern = PATTERN_FIELD;
	ptr->common.common.foreground = COLOR_DARK_JADE;
	ptr->common.common.background = COLOR_ONYX;
	ptr->common.timerFunc = TerrainDescriptor_doNothingTimer;
	ptr->common.initializeFunc = GrassTerrain_initialize;
	ptr->common.name = "Grass";
}

static void TreeTerrainDescriptor_initialize(TERRAINDESCRIPTOR* ptr)
{
	assert(ptr);
	ptr->common.common.type.terrainType = TERRAIN_TREE;
	ptr->common.common.pattern = PATTERN_TREE;
	ptr->common.common.foreground = COLOR_JADE;
	ptr->common.common.background = COLOR_ONYX;
	ptr->common.timerFunc = TreeTerrain_doTimer;
	ptr->common.initializeFunc = TreeTerrain_initialize;
	ptr->common.name = "Tree";
}

static void RockTerrainDescriptor_initialize(TERRAINDESCRIPTOR* ptr)
{
	assert(ptr);
	ptr->common.common.type.terrainType = TERRAIN_ROCK;
	ptr->common.common.pattern = PATTERN_ROCK;
	ptr->common.common.foreground = COLOR_DARK_SILVER;
	ptr->common.common.background = COLOR_ONYX;
	ptr->common.timerFunc = TerrainDescriptor_doNothingTimer;
	ptr->common.initializeFunc = RockTerrain_initialize;
	ptr->common.name = "Rock";
}

static void FlintrockTerrainDescriptor_initialize(TERRAINDESCRIPTOR* ptr)
{
	assert(ptr);
	ptr->common.common.type.terrainType = TERRAIN_FLINT_ROCK;
	ptr->common.common.pattern = PATTERN_ROCK;
	ptr->common.common.foreground = COLOR_DARKER_SILVER;
	ptr->common.common.background = COLOR_ONYX;
	ptr->common.timerFunc = TerrainDescriptor_doNothingTimer;
	ptr->common.initializeFunc = FlintrockTerrain_initialize;
	ptr->common.name = "Flintrock";
}

static void BushTerrainDescriptor_initialize(TERRAINDESCRIPTOR* ptr)
{
	assert(ptr);
	ptr->common.common.type.terrainType = TERRAIN_BUSH;
	ptr->common.common.pattern = PATTERN_BUSH;
	ptr->common.common.foreground = COLOR_DARK_JADE;
	ptr->common.common.background = COLOR_ONYX;
	ptr->common.timerFunc = BushTerrain_doTimer;
	ptr->common.initializeFunc = BushTerrain_initialize;
	ptr->common.name = "Bush";
}

static void RabbitHoleTerrainDescriptor_initialize(TERRAINDESCRIPTOR* ptr)
{
	assert(ptr);
	ptr->common.common.type.terrainType = TERRAIN_RABBIT_HOLE;
	ptr->common.common.pattern = PATTERN_HOLE;
	ptr->common.common.foreground = COLOR_COPPER;
	ptr->common.common.background = COLOR_ONYX;
	ptr->common.timerFunc = TerrainDescriptor_doNothingTimer;
	ptr->common.initializeFunc = RabbitHole_initialize;
	ptr->common.name = "Rabbit Hole";
}

static void WaterTerrainDescriptor_initialize(TERRAINDESCRIPTOR* ptr)
{
	assert(ptr);
	ptr->common.common.type.terrainType = TERRAIN_WATER;
	ptr->common.common.pattern = PATTERN_FIELD;
	ptr->common.common.foreground = COLOR_RUBY;
	ptr->common.common.background = COLOR_DARK_RUBY;
	ptr->common.timerFunc = TerrainDescriptor_doNothingTimer;
	ptr->common.initializeFunc = WaterTerrain_initialize;
	ptr->common.name = "Water";
}

static void StumpTerrainDescriptor_initialize(TERRAINDESCRIPTOR* ptr)
{
	assert(ptr);
	ptr->common.common.type.terrainType = TERRAIN_STUMP;
	ptr->common.common.pattern = PATTERN_FIELD;
	ptr->common.common.foreground = COLOR_JADE;
	ptr->common.common.background = COLOR_ONYX;
	ptr->common.timerFunc = TerrainDescriptor_doNothingTimer;
	ptr->common.initializeFunc = StumpTerrain_initialize;
	ptr->common.name = "Stump";
}

static void RubbleTerrainDescriptor_initialize(TERRAINDESCRIPTOR* ptr)
{
	assert(ptr);
	ptr->common.common.type.terrainType = TERRAIN_RUBBLE;
	ptr->common.common.pattern = PATTERN_FIELD;
	ptr->common.common.foreground = COLOR_DARK_SILVER;
	ptr->common.common.background = COLOR_ONYX;
	ptr->common.timerFunc = TerrainDescriptor_doNothingTimer;
	ptr->common.initializeFunc = RubbleTerrain_initialize;
	ptr->common.name = "Rubble";
}

static void DeadBushTerrainDescriptor_initialize(TERRAINDESCRIPTOR* ptr)
{
	assert(ptr);
	ptr->common.common.type.terrainType = TERRAIN_DEAD_BUSH;
	ptr->common.common.pattern = PATTERN_BUSH;
	ptr->common.common.foreground = COLOR_DARK_COPPER;
	ptr->common.common.background = COLOR_ONYX;
	ptr->common.timerFunc = TerrainDescriptor_doNothingTimer;
	ptr->common.initializeFunc = DeadBushTerrain_initialize;
	ptr->common.name = "Dead Bush";
}

static void BerryStarterTerrainDescriptor_initialize(TERRAINDESCRIPTOR* ptr)
{
	assert(ptr);
	ptr->common.common.type.terrainType = TERRAIN_BERRY_STARTER;
	ptr->common.common.pattern = PATTERN_BERRYSEED;
	ptr->common.common.foreground = COLOR_DARK_COPPER;
	ptr->common.common.background = COLOR_ONYX;
	ptr->common.timerFunc = BerryStarterTerrain_doTimer;
	ptr->common.initializeFunc = BerryStarterTerrain_initialize;
	ptr->common.name = "Berry Seed";
}

static void DirtTerrainDescriptor_initialize(TERRAINDESCRIPTOR* ptr)
{
	assert(ptr);
	ptr->common.common.type.terrainType = TERRAIN_DIRT;
	ptr->common.common.pattern = PATTERN_FIELD;
	ptr->common.common.foreground = COLOR_DARK_COPPER;
	ptr->common.common.background = COLOR_ONYX;
	ptr->common.timerFunc = DirtTerrain_doTimer;
	ptr->common.initializeFunc = DirtTerrain_initialize;
	ptr->common.name = "Dirt";
}

static void AcornTerrainDescriptor_initialize(TERRAINDESCRIPTOR* ptr)
{
	assert(ptr);
	ptr->common.common.type.terrainType = TERRAIN_ACORN;
	ptr->common.common.pattern = PATTERN_ACORN;
	ptr->common.common.foreground = COLOR_DARK_COPPER;
	ptr->common.common.background = COLOR_ONYX;
	ptr->common.timerFunc = AcornTerrain_doTimer;
	ptr->common.initializeFunc = AcornTerrain_initialize;
	ptr->common.name = "Sapling";
}

static TerrainDescriptorInitializFunc terrainDescriptorInitializeFuncs[TERRAIN_COUNT] =
{
	GrassTerrainDescriptor_initialize,
	TreeTerrainDescriptor_initialize,
	RockTerrainDescriptor_initialize,
	FlintrockTerrainDescriptor_initialize,
	BushTerrainDescriptor_initialize,
	RabbitHoleTerrainDescriptor_initialize,
	WaterTerrainDescriptor_initialize,
	StumpTerrainDescriptor_initialize,
	RubbleTerrainDescriptor_initialize,
	DeadBushTerrainDescriptor_initialize,
	BerryStarterTerrainDescriptor_initialize,
	DirtTerrainDescriptor_initialize,
	AcornTerrainDescriptor_initialize
};

void TerrainDescriptors_initialize(void)
{
	for (int index = 0; index < TERRAIN_COUNT; ++index)
	{
		assert(terrainDescriptorInitializeFuncs[index]);
		terrainDescriptorInitializeFuncs[index](TerrainDescriptors_getDescriptor(index));
	}
}

TERRAINDESCRIPTOR* TerrainDescriptors_getDescriptor(TERRAINTYPE terrainType)
{
	assert(terrainType >= TERRAIN_FIRST && terrainType < TERRAIN_COUNT);
	return &terrainDescriptors[terrainType];
}

void TerrainDescriptors_initializeInstance(TERRAINTYPE terrainType, TERRAIN* terrain)
{
	assert(terrainType >= TERRAIN_FIRST && terrainType < TERRAIN_COUNT);
	assert(terrain);
	TerrainDescriptor_initializeInstance(TerrainDescriptors_getDescriptor(terrainType), terrain);
}

const char* TerrainDescriptors_getName(TERRAINTYPE terrainType)
{
	return TerrainDescriptor_getName(TerrainDescriptors_getDescriptor(terrainType));
}