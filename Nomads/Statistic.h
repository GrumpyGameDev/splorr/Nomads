#ifndef STATISTIC_H
#define STATISTIC_H

#ifdef __cplusplus
extern "C" {
#endif

#include "StatisticType.h"

typedef struct Statistic STATISTIC;

void Statistic_setCurrent(STATISTIC* ptr, int current);
void Statistic_setMinimum(STATISTIC* ptr, int minimum);
void Statistic_setMaximum(STATISTIC* ptr, int maximum);
int Statistic_hasAtLeast(STATISTIC* ptr, int value);
void Statistic_changeBy(STATISTIC* ptr, int delta);

int Statistic_getCurrent(STATISTIC* ptr);
int Statistic_getMinimum(STATISTIC* ptr);
int Statistic_getMaximum(STATISTIC* ptr);

#ifdef __cplusplus
}
#endif

#endif

