#include "VerbToolbar_internals.h"
#include "View_internals.h"
#include "Constants.h"
#include <assert.h>
#include "Game.h"
#include "PatternPlotter.h"
#include "VerbDescriptors.h"

#define VERBTOOLBAR_OFFSET_X (0)
#define VERBTOOLBAR_OFFSET_Y (SCALE_HEIGHT(SCALE_LARGE)-PATTERN_HEIGHT)
#define VERBTOOLBAR_WIDTH (SCALE_WIDTH(SCALE_LARGE))
#define VERBTOOLBAR_HEIGHT (PATTERN_HEIGHT)

void VerbToolbar_draw(union View* ptr)
{
	assert(ptr);
	VerbToolbar_refresh(View_getVerbToolbar(ptr));
	PIXELPLOTTERSTATE old;
	PixelPlotter_getState(&old);
	PixelPlotter_setState(View_getPixelPlotterState(ptr));

	PixelPlotter_plotRect(0, 0, VERBTOOLBAR_WIDTH, VERBTOOLBAR_HEIGHT, COLOR_ONYX);

	VERBTOOLBAR* toolbar = View_getVerbToolbar(ptr);
	SCROLLER* scroller = VerbToolbar_getScroller(toolbar);

	for (int index = 0, plotX = VERBTOOLBAR_WIDTH/2 - PATTERN_WIDTH/2 - Scroller_getIndex(scroller) * PATTERN_WIDTH; index < Scroller_getItemCount(scroller); ++index, plotX+=PATTERN_WIDTH)
	{
		if (plotX > -PATTERN_WIDTH && plotX < VERBTOOLBAR_WIDTH)
		{
			if (index == Scroller_getIndex(scroller))
			{
				PatternPlotter_plotPattern(plotX, 0, PATTERN_SELECTOR, COLOR_LIGHTEST, COLOR_TRANSPARENT);
			}
			VERBTYPE verb = toolbar->verbs[index];
			VERBDESCRIPTOR* descriptor = VerbDescriptors_getDescriptor(verb);
			PatternPlotter_plotPattern(plotX, 0, VerbDescriptor_getPattern(descriptor), VerbDescriptor_getForeground(descriptor), VerbDescriptor_getBackground(descriptor));
		}
	}

	PixelPlotter_setState(&old);
}

int VerbToolbar_process(union View* ptr, VIEWCOMMANDTYPE command)
{
	assert(ptr);
	assert(command >= VIEWCOMMAND_FIRST && command < VIEWCOMMAND_COUNT);
	VERBTOOLBAR* toolbar = View_getVerbToolbar(ptr);
	SCROLLER* scroller = VerbToolbar_getScroller(toolbar);
	switch (command)
	{
	case VIEWCOMMAND_BUTTON_NEXT:
		Scroller_nextIndex(scroller);
		Scroller_refresh(scroller);
		return 0;
	case VIEWCOMMAND_BUTTON_PREVIOUS:
		Scroller_previousIndex(scroller);
		Scroller_refresh(scroller);
		return 0;
	default:
		return 0;
	}
}

void VerbToolbar_initialize(union View* ptr)
{
	assert(ptr);
	PixelPlotterState_initialize(View_getPixelPlotterState(ptr),
		VERBTOOLBAR_OFFSET_X,
		VERBTOOLBAR_OFFSET_Y,
		SCALE_LARGE,
		SCALE_LARGE,
		1,
		VERBTOOLBAR_WIDTH,
		VERBTOOLBAR_HEIGHT);
	ptr->viewCommon.viewType = VIEW_VERBTOOLBAR;
	ptr->viewCommon.enabled = 1;

	VERBTOOLBAR* toolbar = View_getVerbToolbar(ptr);
	SCROLLER* scroller = VerbToolbar_getScroller(toolbar);
	Scroller_initialize(scroller, 0, VERB_COUNT);
}

void VerbToolbar_refresh(VERBTOOLBAR* ptr)
{
	assert(ptr);
	TAGON* tagon = Game_getTagon();
	VERBTYPE verbCount = VERB_FIRST;
	for (VERBTYPE index = VERB_FIRST; index < VERB_COUNT; ++index)
	{
		if (Tagon_canDoVerb(tagon, index))
		{
			ptr->verbs[verbCount++] = index;
		}
	}
	Scroller_setItemCount(VerbToolbar_getScroller(ptr), verbCount);
	Scroller_refresh(VerbToolbar_getScroller(ptr));
	Tagon_setVerb(tagon, ptr->verbs[Scroller_getIndex(VerbToolbar_getScroller(ptr))]);
}

VERBTYPE VerbToolbar_getVerb(VERBTOOLBAR* ptr)
{
	assert(ptr);
	return ptr->verbs[Scroller_getIndex(VerbToolbar_getScroller(ptr))];
}

SCROLLER* VerbToolbar_getScroller(VERBTOOLBAR* ptr)
{
	assert(ptr);
	return &(ptr->scroller);
}


