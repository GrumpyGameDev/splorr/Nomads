#ifndef COLOR_H
#define COLOR_H

#ifdef __cplusplus
extern "C" {
#endif

typedef struct Color COLOR;

unsigned char Color_getRed(COLOR* ptr);
unsigned char Color_getGreen(COLOR* ptr);
unsigned char Color_getBlue(COLOR* ptr);
unsigned char Color_getAlpha(COLOR* ptr);

#ifdef __cplusplus
}
#endif

#endif
