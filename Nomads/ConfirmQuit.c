#include "View_internals.h"
#include "TextPlotter.h"
#include "PixelPlotter_internals.h"
#include "Views.h"
#include "Constants.h"
#include "ConfirmQuit_internals.h"
#include <assert.h>

#define CONFIRMQUIT_WIDTH (13*PATTERN_WIDTH)
#define CONFIRMQUIT_HEIGHT (24)

static const char* confirmQuitItemTexts[CONFIRMQUITITEM_COUNT] = { "     No      ","     Yes     " };
static int confirmQuitItemYs[CONFIRMQUITITEM_COUNT] = { 8,16 };

void ConfirmQuit_initialize(union View* ptr)
{
	assert(ptr);
	PixelPlotterState_initialize(View_getPixelPlotterState(ptr),
	SCALE_CENTER_X(SCALE_LARGE,CONFIRMQUIT_WIDTH),
	SCALE_CENTER_Y(SCALE_LARGE,CONFIRMQUIT_HEIGHT),
	SCALE_LARGE,
	SCALE_LARGE,
	1,
	CONFIRMQUIT_WIDTH,
	CONFIRMQUIT_HEIGHT);
	ptr->confirmQuit.viewCommon.viewType = VIEW_CONFIRM_QUIT;
	ptr->confirmQuit.viewCommon.enabled = 0;
	ptr->confirmQuit.currentItem = CONFIRMQUITITEM_NO;
}

void ConfirmQuit_draw(union View* ptr)
{
	assert(ptr);
	PIXELPLOTTERSTATE old;
	PixelPlotter_getState(&old);
	PixelPlotter_setState(View_getPixelPlotterState(ptr));

	TextPlotter_plotText(0, 0, "Confirm Quit?", COLOR_ONYX, COLOR_SILVER);

	COLORTYPE foreground;
	COLORTYPE background;
	for (int index = 0; index < CONFIRMQUITITEM_COUNT; ++index)
	{
		foreground = (index == ptr->confirmQuit.currentItem) ? (COLOR_AMETHYST) : (COLOR_DARK_AMETHYST);
		background = (index == ptr->confirmQuit.currentItem) ? (COLOR_ONYX) : (COLOR_ONYX);
		TextPlotter_plotText(0, confirmQuitItemYs[index], confirmQuitItemTexts[index], foreground, background);
	}

	PixelPlotter_setState(&old);
}

int ConfirmQuit_process(union View* ptr, VIEWCOMMANDTYPE command)
{
	assert(ptr);
	switch (command)
	{
	case VIEWCOMMAND_MOVE_UP:
		ptr->confirmQuit.currentItem += (CONFIRMQUITITEM_COUNT - 1);
		ptr->confirmQuit.currentItem %= CONFIRMQUITITEM_COUNT;
		return 1;
	case VIEWCOMMAND_MOVE_DOWN:
		ptr->confirmQuit.currentItem++;
		ptr->confirmQuit.currentItem %= CONFIRMQUITITEM_COUNT;
		return 1;
	case VIEWCOMMAND_BUTTON_A:
		switch (ptr->confirmQuit.currentItem)
		{
		case CONFIRMQUITITEM_NO:
			Views_disable(VIEW_CONFIRM_QUIT);
			Views_enable(VIEW_MAIN_MENU);
			break;
		case CONFIRMQUITITEM_YES:
			Views_disableAll();
			break;
		}
		return 1;
	default:
		return 0;
	}
}


