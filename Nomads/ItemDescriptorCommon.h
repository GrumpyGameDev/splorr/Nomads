#ifndef ITEM_DESCRIPTOR_COMMON_H
#define ITEM_DESCRIPTOR_COMMON_H

#ifdef __cplusplus
extern "C" {
#endif

#include "DescriptorCommon.h"
#include "VerbType.h"

typedef struct ItemDescriptorCommon ITEMDESCRIPTORCOMMON;

ITEMTYPE ItemDescriptorCommon_getType(ITEMDESCRIPTORCOMMON* ptr);
PATTERNTYPE ItemDescriptorCommon_getPattern(ITEMDESCRIPTORCOMMON* ptr);
COLORTYPE ItemDescriptorCommon_getForeground(ITEMDESCRIPTORCOMMON* ptr);
COLORTYPE ItemDescriptorCommon_getBackground(ITEMDESCRIPTORCOMMON* ptr);
const char* ItemDescriptorCommon_getName(ITEMDESCRIPTORCOMMON* ptr);


#ifdef __cplusplus
}
#endif

#endif

