#ifndef GENERATOR_H
#define GENERATOR_H

#ifdef __cplusplus
extern "C" {
#endif

	typedef struct GeneratorEntry{
		int value;
		int weight;
	} GENERATORENTRY;

	typedef struct {
		int count;
		const GENERATORENTRY* entries;
	} GENERATOR;

	void Generator_initialize(GENERATOR* ptr, int count, const GENERATORENTRY* entries);
	int Generator_generate(GENERATOR* ptr);


#ifdef __cplusplus
}
#endif

#endif

