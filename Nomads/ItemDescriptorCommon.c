#include "ItemDescriptorCommon_internals.h"
#include <assert.h>

DESCRIPTORCOMMON* ItemDescriptorCommon_getCommon(ITEMDESCRIPTORCOMMON* ptr)
{
	assert(ptr);
	return &(ptr->common);
}

ITEMTYPE ItemDescriptorCommon_getType(ITEMDESCRIPTORCOMMON* ptr)
{
	return DescriptorCommon_getItemType(ItemDescriptorCommon_getCommon(ptr));
}

PATTERNTYPE ItemDescriptorCommon_getPattern(ITEMDESCRIPTORCOMMON* ptr)
{
	assert(ptr);
	return DescriptorCommon_getPattern(ItemDescriptorCommon_getCommon(ptr));
}

COLORTYPE ItemDescriptorCommon_getForeground(ITEMDESCRIPTORCOMMON* ptr)
{
	assert(ptr);
	return DescriptorCommon_getForeground(ItemDescriptorCommon_getCommon(ptr));
}

COLORTYPE ItemDescriptorCommon_getBackground(ITEMDESCRIPTORCOMMON* ptr)
{
	assert(ptr);
	return DescriptorCommon_getBackground(ItemDescriptorCommon_getCommon(ptr));
}

const char* ItemDescriptorCommon_getName(ITEMDESCRIPTORCOMMON* ptr)
{
	assert(ptr);
	return ptr->name;
}


