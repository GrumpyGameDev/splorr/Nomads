#ifndef CRAFT_SCREEN_H
#define CRAFT_SCREEN_H

#ifdef __cplusplus
extern "C" {
#endif

#include "ViewCommandType.h"
#include "Scroller.h"

	typedef enum CraftScreenScrollerType {
		CRAFTSCREENSCROLLER_RECIPE,
		CRAFTSCREENSCROLLER_INVENTORY,
		CRAFTSCREENSCROLLER_COUNT,
		CRAFTSCREENSCROLLER_FIRST = CRAFTSCREENSCROLLER_RECIPE
	} CRAFTSCREENSCROLLERTYPE;

	typedef struct CraftScreen CRAFTSCREEN;

	union View;

	void CraftScreen_draw(union View*);
	int CraftScreen_process(union View*, VIEWCOMMANDTYPE);
	void CraftScreen_initialize(union View*);

	void CraftScreen_refresh(CRAFTSCREEN* ptr);
	SCROLLER* CraftScreen_getScroller(CRAFTSCREEN* ptr, CRAFTSCREENSCROLLERTYPE scrollerType);
	CRAFTSCREENSCROLLERTYPE CraftScreen_getCurrentScroller(CRAFTSCREEN* ptr);
	void CraftScreen_toggleCurrentScroller(CRAFTSCREEN* ptr);

#ifdef __cplusplus
}
#endif

#endif

