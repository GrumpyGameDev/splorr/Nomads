#ifndef BERRY_DESCRIPTOR_INTERNALS_H
#define BERRY_DESCRIPTOR_INTERNALS_H

#ifdef __cplusplus
extern "C" {
#endif

#include "ItemDescriptorCommon_internals.h"
#include "BerryType.h"

	typedef struct BerryDescriptor {
		ITEMDESCRIPTORCOMMON common;
		BERRYTYPE berryType;
		int energy;
	} BERRYDESCRIPTOR;

	ITEMDESCRIPTORCOMMON* BerryDescriptor_getItemCommon(BERRYDESCRIPTOR* ptr);
	int BerryDescriptor_getEnergy(BERRYDESCRIPTOR* ptr);
	void BerryDescriptor_use(union ItemDescriptor* descriptor, struct Tagon* tagon, VERBTYPE verb);


#ifdef __cplusplus
}
#endif

#endif

