#include "Terrain_internals.h"
#include "TerrainDescriptors.h"
#include "MapCell.h"
#include <assert.h>

void Terrain_initialize(TERRAIN* ptr, TERRAINDESCRIPTOR* descriptor)
{
	assert(ptr);
	assert(descriptor);
	TerrainDescriptor_initializeInstance(descriptor,ptr);
}

void Terrain_doTimers(struct MapCell* ptr)
{
	assert(ptr);
	TerrainDescriptor_doTimers(TerrainDescriptors_getDescriptor(Terrain_getType(MapCell_getTerrain(ptr))),ptr);
}

TERRAINTYPE Terrain_getType(TERRAIN* ptr)
{
	assert(ptr);
	return ptr->common.type;
}

TREETERRAIN* Terrain_getTree(TERRAIN* ptr)
{
	assert(ptr);
	return &(ptr->tree);
}

BUSHTERRAIN* Terrain_getBush(TERRAIN* ptr)
{
	assert(ptr);
	return &(ptr->bush);
}

ROCKTERRAIN* Terrain_getRock(TERRAIN* ptr)
{
	assert(ptr);
	return &(ptr->rock);
}

FLINTROCKTERRAIN* Terrain_getFlintrock(TERRAIN* ptr)
{
	assert(ptr);
	return &(ptr->flintrock);
}

BERRYSTARTERTERRAIN* Terrain_getBerryStarter(TERRAIN* ptr)
{
	assert(ptr);
	return &(ptr->berryStarter);
}

DIRTTERRAIN* Terrain_getDirt(TERRAIN* ptr)
{
	assert(ptr);
	return &(ptr->dirt);
}

ACORNTERRAIN* Terrain_getAcorn(TERRAIN* ptr)
{
	assert(ptr);
	return &(ptr->acorn);
}

