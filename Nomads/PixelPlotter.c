#include "PixelPlotter_internals.h"
#include <string.h>
#include "Colors.h"
#include "SDL.h"
#include <assert.h>

static PIXELPLOTTERSTATE pixelPlotterState = { 0,0,1,1,0,0,0 };
static SDL_Renderer* pixelPlotterRenderer = 0;
static SDL_Rect pixelPlotterRect = { 0,0,1,1 };

void PixelPlotter_setRenderer(void* renderer)
{
	pixelPlotterRenderer = renderer;
}

void* PixelPlotter_getRenderer(void)
{
	return pixelPlotterRenderer;
}

void PixelPlotter_setState(PIXELPLOTTERSTATE* state)
{
	assert(state);
	memcpy(&pixelPlotterState, state, sizeof(PIXELPLOTTERSTATE));
	pixelPlotterRect.w = state->scaleX;
	pixelPlotterRect.h = state->scaleY;
}

void PixelPlotter_getState(PIXELPLOTTERSTATE* state)
{
	assert(state);
	memcpy(state, &pixelPlotterState, sizeof(PIXELPLOTTERSTATE));
}

void PixelPlotter_setOffsetX(int offsetX)
{
	pixelPlotterState.offsetX = offsetX;
}

void PixelPlotter_setOffsetY(int offsetY)
{
	pixelPlotterState.offsetY = offsetY;
}

void PixelPlotter_setScaleX(int scaleX)
{
	pixelPlotterState.scaleX = scaleX;
	pixelPlotterRect.w = scaleX;
}

void PixelPlotter_setScaleY(int scaleY)
{
	pixelPlotterState.scaleY = scaleY;
	pixelPlotterRect.h = scaleY;
}

void PixelPlotter_setClipEnabled(int clipEnabled)
{
	pixelPlotterState.clipEnabled = clipEnabled;
}

void PixelPlotter_setClipWidth(int clipWidth)
{
	pixelPlotterState.clipWidth = clipWidth;
}

void PixelPlotter_setClipHeight(int clipHeight)
{
	pixelPlotterState.clipHeight = clipHeight;
}


int PixelPlotter_getOffsetX(void)
{
	return pixelPlotterState.offsetX;
}

int PixelPlotter_getOffsetY(void)
{
	return pixelPlotterState.offsetY;
}

int PixelPlotter_getScaleX(void)
{
	return pixelPlotterState.scaleX;
}

int PixelPlotter_getScaleY(void)
{
	return pixelPlotterState.scaleX;
}

int PixelPlotter_getClipEnabled(void)
{
	return pixelPlotterState.clipEnabled;
}

int PixelPlotter_getClipWidth(void)
{
	return pixelPlotterState.clipWidth;
}

int PixelPlotter_getClipHeight(void)
{
	return pixelPlotterState.clipHeight;
}

void PixelPlotter_plotPixel(int x, int y, COLORTYPE colorType)
{
	assert(colorType >= COLOR_FIRST && colorType <= COLOR_COUNT);
	if (pixelPlotterState.clipEnabled && (x < 0 || y < 0 || x >= pixelPlotterState.clipWidth || y >= pixelPlotterState.clipHeight))
	{
		return;
	}
	if (colorType == COLOR_TRANSPARENT)
	{
		return;
	}
	Uint8 r, g, b, a;
	pixelPlotterRect.x = x * pixelPlotterState.scaleX + pixelPlotterState.offsetX * pixelPlotterState.scaleX;
	pixelPlotterRect.y = y * pixelPlotterState.scaleY + pixelPlotterState.offsetY * pixelPlotterState.scaleY;
	SDL_GetRenderDrawColor(pixelPlotterRenderer, &r, &g, &b, &a);
	COLOR* color = Colors_getColor(colorType);
	SDL_SetRenderDrawColor(pixelPlotterRenderer,Color_getRed(color), Color_getGreen(color), Color_getBlue(color), Color_getAlpha(color));
	SDL_RenderFillRect(pixelPlotterRenderer, &pixelPlotterRect);
	SDL_SetRenderDrawColor(pixelPlotterRenderer, r, g, b, a);
}

void PixelPlotter_plotRect(int x, int y, int w, int h, COLORTYPE colorType)
{
	assert(colorType >= COLOR_FIRST && colorType <= COLOR_COUNT);
	for (int deltaX = 0; deltaX < w; ++deltaX,++x)
	{
		for (int deltaY = 0; deltaY < h; ++deltaY)
		{
			PixelPlotter_plotPixel(x, y+deltaY, colorType);
		}
	}
}

void PixelPlotterState_initialize(PIXELPLOTTERSTATE* state,int offsetX, int offsetY,int scaleX,int scaleY,int clipEnabled,int clipWidth,int clipHeight)
{
	assert(state);
	state->offsetX=offsetX;
	state->offsetY=offsetY;
	state->scaleX=scaleX;
	state->scaleY=scaleY;
	state->clipEnabled=clipEnabled;
	state->clipWidth=clipWidth;
	state->clipHeight=clipHeight;
}

void PixelPlotter_setScale(int x, int y)
{
	PixelPlotter_setScaleX(x);
	PixelPlotter_setScaleY(y);
}

void PixelPlotter_setOffset(int x, int y)
{
	PixelPlotter_setOffsetX(x);
	PixelPlotter_setOffsetY(y);
}
