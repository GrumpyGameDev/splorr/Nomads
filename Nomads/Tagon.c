#include "Tagon_internals.h"
#include "Utility.h"
#include "ItemDescriptors.h"
#include "VerbDescriptors.h"
#include <assert.h>

STATISTIC* Tagon_getStatistic(TAGON* ptr,STATISTICTYPE statistic)
{
	assert(ptr);
	assert(statistic >= STATISTIC_FIRST && statistic < STATISTIC_COUNT);
	return &(ptr->statistics[statistic]);
}

void Tagon_postInteract(TAGON* ptr)
{
	assert(ptr);
}

void Tagon_changeStatisticBy(TAGON* ptr, STATISTICTYPE statistic, int delta)
{
	assert(ptr);
	assert(statistic >= STATISTIC_FIRST && statistic < STATISTIC_COUNT);
	Statistic_changeBy(Tagon_getStatistic(ptr, statistic), delta);
}

void Tagon_changeInventoryCountBy(TAGON* ptr, ITEMTYPE item, int delta)
{
	assert(ptr);
	assert(item >= ITEM_FIRST && item < ITEM_COUNT);
	Inventory_setCount(Tagon_getInventory(ptr), item, Inventory_getCount(Tagon_getInventory(ptr), item) + delta);
}

INVENTORY* Tagon_getInventory(TAGON* ptr)
{
	assert(ptr);
	return &(ptr->inventory);
}

void Tagon_useVerb(TAGON* ptr, VERBTYPE verb)
{
	assert(ptr);
	assert(verb >= VERB_FIRST && verb < VERB_COUNT);
	VERBDESCRIPTOR* descriptor = VerbDescriptors_getDescriptor(verb);
	VerbDescriptor_use(descriptor, ptr);
}

int Tagon_hasInventoryCount(TAGON* ptr, ITEMTYPE item)
{
	assert(ptr);
	assert(item >= ITEM_FIRST && item < ITEM_COUNT);
	return Inventory_hasCount(Tagon_getInventory(ptr), item);
}

int Tagon_getInventoryCount(TAGON* ptr, ITEMTYPE item)
{
	assert(ptr);
	assert(item >= ITEM_FIRST && item < ITEM_COUNT);
	return Inventory_getCount(Tagon_getInventory(ptr), item);
}

void Tagon_setExertion(TAGON * ptr, EXERTIONTYPE exertion)
{
	assert(ptr);
	ptr->exertion = exertion;
}

EXERTIONTYPE Tagon_getExertion(TAGON* ptr)
{
	assert(ptr);
	return ptr->exertion;
}

void Tagon_setVerb(TAGON* ptr, VERBTYPE verb)
{
	assert(ptr);
	assert(verb >= VERB_FIRST && verb < VERB_COUNT);
	ptr->currentVerb = verb;
}

VERBTYPE Tagon_getVerb(TAGON* ptr)
{
	assert(ptr);
	return ptr->currentVerb;
}

int Tagon_canDoVerb(TAGON* ptr, VERBTYPE verb)
{
	assert(ptr);
	assert(verb >= VERB_FIRST && verb < VERB_COUNT);
	VERBDESCRIPTOR* descriptor = VerbDescriptors_getDescriptor(verb);
	return VerbDescriptor_canDoVerb(descriptor, Tagon_getInventory(ptr));
}

int Tagon_canDoCurrentVerb(TAGON* ptr)
{
	assert(ptr);
	return Tagon_canDoVerb(ptr, Tagon_getVerb(ptr));
}

int Tagon_getCurrentStatistic(TAGON* ptr, STATISTICTYPE statistic)
{
	return Statistic_getCurrent(Tagon_getStatistic(ptr, statistic));
}