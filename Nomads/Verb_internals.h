#ifndef VERB_INTERNALS_H
#define VERB_INTERNALS_H

#ifdef __cplusplus
extern "C" {
#endif

#include "VerbDescriptor_internals.h"
#include "Terrain.h"

void ShovelVerb_do(TAGON* tagon, TERRAIN* terrain, VERBTYPE verb);
void PlaceGravelVerb_do(TAGON* tagon, TERRAIN* terrain);

void LookVerb_use(VERBDESCRIPTOR* ptr, TAGON* tagon);
void TakeVerb_use(VERBDESCRIPTOR* ptr, TAGON* tagon);
void PickBerryVerb_use(VERBDESCRIPTOR* ptr, TAGON* tagon);
void TakeLogVerb_use(VERBDESCRIPTOR* ptr, TAGON* tagon);
void TalkVerb_use(VERBDESCRIPTOR* ptr, TAGON* tagon);
void AxeVerb_use(VERBDESCRIPTOR* ptr, TAGON* tagon);
void PickVerb_use(VERBDESCRIPTOR* ptr, TAGON* tagon);
void SpearVerb_use(VERBDESCRIPTOR* ptr, TAGON* tagon);
void ShovelVerb_use(VERBDESCRIPTOR* ptr, TAGON* tagon);
void PlantSeedVerb_use(VERBDESCRIPTOR* ptr, TAGON* tagon);
void PlantAcornVerb_use(VERBDESCRIPTOR* ptr, TAGON* tagon);
void PlaceGravelVerb_use(VERBDESCRIPTOR* ptr, TAGON* tagon);
void EatBerryVerb_use(VERBDESCRIPTOR* ptr, TAGON* tagon);



#ifdef __cplusplus
}
#endif

#endif

