#ifndef FLINTROCK_TERRAIN_H
#define FLINTROCK_TERRAIN_H

#ifdef __cplusplus
extern "C" {
#endif

	typedef struct FlintrockTerrain FLINTROCKTERRAIN;

	int FlintrockTerrain_getFlintLeft(FLINTROCKTERRAIN* ptr);
	int FlintrockTerrain_hasFlintLeft(FLINTROCKTERRAIN* ptr);
	void FlintrockTerrain_setFlintLeft(FLINTROCKTERRAIN* ptr, int flintLeft);
	void FlintrockTerrain_changeFlintLeftBy(FLINTROCKTERRAIN* ptr, int delta);

#ifdef __cplusplus
}
#endif

#endif

