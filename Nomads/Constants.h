#ifndef CONSTANTS_H
#define CONSTANTS_H

#ifdef __cplusplus
extern "C" {
#endif

#define PATTERN_HEIGHT (8)
#define PATTERN_WIDTH (8)

#define MAP_COLUMNS (256)
#define MAP_ROWS (256)

#define ZOOM_FACTOR (4)
#define BASE_WIDTH (320)
#define BASE_HEIGHT (240)

#define SCREEN_WIDTH (BASE_WIDTH * ZOOM_FACTOR)
#define SCREEN_HEIGHT (BASE_HEIGHT * ZOOM_FACTOR)
#define SCALE_SMALL (SCREEN_WIDTH/BASE_WIDTH)
#define SCALE_LARGE (SCALE_SMALL * 2)

#define SCALE_WIDTH(s) (SCREEN_WIDTH/(s))
#define SCALE_HEIGHT(s) (SCREEN_HEIGHT/(s))

#define SCALE_CENTER_X(s,w) (SCALE_WIDTH(s)/2-(w)/2)
#define SCALE_CENTER_Y(s,h) (SCALE_HEIGHT(s)/2-(h)/2)

#ifdef __cplusplus
}
#endif

#endif
