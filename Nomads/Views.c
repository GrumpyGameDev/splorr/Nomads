#include "Views.h"
#include "View_internals.h"
#include "MainMenu.h"
#include "StatsView.h"
#include "MessageFeed.h"
#include "QuickHelp.h"
#include "MapScreen.h"
#include "ConfirmQuit.h"
#include <assert.h>

static VIEW views[VIEW_COUNT];

typedef void(*ViewInitializeFunc)(VIEW*);
typedef void(*ViewCleanUpFunc)(VIEW*);

static ViewInitializeFunc viewInitializeFuncs[VIEW_COUNT] =
{
	MapScreen_initialize,
	StatsView_initialize,
	MessageFeed_initialize,
	VerbToolbar_initialize,
	QuickHelp_initialize,
	CraftScreen_initialize,
	InventoryScreen_initialize,
	MainMenu_initialize,
	ConfirmQuit_initialize
};

void Views_initialize(void)
{
	for (int index = 0; index < VIEW_COUNT; ++index)
	{
		viewInitializeFuncs[index](&views[index]);
	}
}

VIEW* Views_getView(VIEWTYPE viewType)
{
	assert(viewType >= VIEW_FIRST && viewType < VIEW_COUNT);
	return &views[viewType];
}

void Views_draw(void)
{
	for (int index = 0; index < VIEW_COUNT; ++index)
	{
		View_draw(Views_getView(index));
	}
}

void Views_enable(VIEWTYPE viewType)
{
	assert(viewType >= VIEW_FIRST && viewType < VIEW_COUNT);
	View_enable(Views_getView(viewType));
}

void Views_disable(VIEWTYPE viewType)
{
	assert(viewType >= VIEW_FIRST && viewType < VIEW_COUNT);
	View_disable(Views_getView(viewType));
}

int Views_isEnabled(VIEWTYPE viewType)
{
	assert(viewType >= VIEW_FIRST && viewType < VIEW_COUNT);
	return View_isEnabled(Views_getView(viewType));
}

void Views_enableAll(void)
{
	for (int index = 0; index < VIEW_COUNT; ++index)
	{
		View_enable(Views_getView(index));
	}
}

void Views_disableAll(void)
{
	for (int index = 0; index < VIEW_COUNT; ++index)
	{
		View_disable(Views_getView(index));
	}
}

void Views_process(VIEWCOMMANDTYPE command)
{
	for (int index = VIEW_COUNT-1; index >=0 ; --index)
	{
		if (View_process(Views_getView(index), command))
		{
			return;
		}
	}
}

int Views_countEnabled(void)
{
	int tally = 0;
	for (int index = 0; index < VIEW_COUNT; ++index)
	{
		if (View_isEnabled(Views_getView(index)))
		{
			tally++;
		}
	}
	return tally;
}


