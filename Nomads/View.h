#ifndef VIEW_H
#define VIEW_H

#ifdef __cplusplus
extern "C" {
#endif

#include "ViewType.h"
#include "PixelPlotter.h"
#include "ViewCommandType.h"
#include "CraftScreen.h"
#include "InventoryScreen.h"
#include "VerbToolbar.h"

typedef union View VIEW;

void View_draw(VIEW* ptr);
int View_process(VIEW* ptr, VIEWCOMMANDTYPE command);

void View_enable(VIEW* ptr);
void View_disable(VIEW* ptr);
int View_isEnabled(VIEW* ptr);
VIEWTYPE View_getType(VIEW* ptr);
PIXELPLOTTERSTATE* View_getPixelPlotterState(VIEW* ptr);

CRAFTSCREEN* View_getCraftScreen(VIEW* ptr);
INVENTORYSCREEN* View_getInventoryScreen(VIEW* ptr);
VERBTOOLBAR* View_getVerbToolbar(VIEW* ptr);

#ifdef __cplusplus
}
#endif

#endif

