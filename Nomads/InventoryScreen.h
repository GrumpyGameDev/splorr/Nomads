#ifndef INVENTORY_SCREEN_H
#define INVENTORY_SCREEN_H

#ifdef __cplusplus
extern "C" {
#endif

#include "ViewCommandType.h"
#include "Scroller.h"

	typedef enum InventoryScreenScrollerType {
		INVENTORYSCREENSCROLLER_IN_HAND,
		INVENTORYSCREENSCROLLER_ON_GROUND,
		INVENTORYSCREENSCROLLER_COUNT,
		INVENTORYSCREENSCROLLER_FIRST = INVENTORYSCREENSCROLLER_IN_HAND
	} INVENTORYSCREENSCROLLERTYPE;

	typedef struct InventoryScreen INVENTORYSCREEN;

	union View;

	void InventoryScreen_draw(union View*);
	int InventoryScreen_process(union View*, VIEWCOMMANDTYPE);
	void InventoryScreen_initialize(union View*);

	void InventoryScreen_refresh(INVENTORYSCREEN* ptr);
	SCROLLER* InventoryScreen_getScroller(INVENTORYSCREEN* ptr, INVENTORYSCREENSCROLLERTYPE scrollerType);
	INVENTORYSCREENSCROLLERTYPE InventoryScreen_getCurrentScroller(INVENTORYSCREEN* ptr);
	void InventoryScreen_toggleCurrentScroller(INVENTORYSCREEN* ptr);

#ifdef __cplusplus
}
#endif

#endif

