#include "VerbDescriptors.h"
#include "VerbDescriptor_internals.h"
#include "Verb_internals.h"
#include <assert.h>
#include "Game.h"

static VERBDESCRIPTOR verbDescriptors[VERB_COUNT] = { 0 };

typedef void(*VerbDescriptorInitializeFunc)(VERBDESCRIPTOR*);

static void LookInitializeFunc(VERBDESCRIPTOR* ptr)
{
	assert(ptr);
	ptr->common.type.verbType = VERB_LOOK;
	ptr->common.pattern = PATTERN_EYE;
	ptr->common.foreground = COLOR_SILVER;
	ptr->common.background = COLOR_TRANSPARENT;
	ptr->item = ITEM_NONE;
	ptr->caption = "Look";
	ptr->useFunc = LookVerb_use;
}

static void PickBerryInitializeFunc(VERBDESCRIPTOR* ptr)
{
	assert(ptr);
	ptr->common.type.verbType = VERB_PICK_BERRY;
	ptr->common.pattern = PATTERN_BUSH;
	ptr->common.foreground = COLOR_DARK_JADE;
	ptr->common.background = COLOR_TRANSPARENT;
	ptr->item = ITEM_NONE;
	ptr->caption = "Take";
	ptr->energy = 0;
	ptr->useFunc = PickBerryVerb_use;
}

static void TakeLogInitializeFunc(VERBDESCRIPTOR* ptr)
{
	assert(ptr);
	ptr->common.type.verbType = VERB_TAKE_LOG;
	ptr->common.pattern = PATTERN_LOG;
	ptr->common.foreground = COLOR_DARK_COPPER;
	ptr->common.background = COLOR_TRANSPARENT;
	ptr->item = ITEM_NONE;
	ptr->caption = "Take";
	ptr->energy = 1;
	ptr->useFunc = TakeLogVerb_use;
}

static void TalkInitializeFunc(VERBDESCRIPTOR* ptr)
{
	assert(ptr);
	ptr->common.type.verbType = VERB_TALK;
	ptr->common.pattern = PATTERN_MOUTH;
	ptr->common.foreground = COLOR_SILVER;
	ptr->common.background = COLOR_TRANSPARENT;
	ptr->item = ITEM_NONE;
	ptr->caption = "Talk";
	ptr->useFunc = TalkVerb_use;
}

static void  UseWoodAxeInitializeFunc(VERBDESCRIPTOR* ptr)
{
	assert(ptr);
	ptr->common.type.verbType = VERB_USE_WOOD_AXE;
	ptr->common.pattern = PATTERN_AXE;
	ptr->common.foreground = COLOR_DARK_COPPER;
	ptr->common.background = COLOR_TRANSPARENT;
	ptr->item = ITEM_WOOD_AXE;
	ptr->caption = "Use Wooden Axe";
	ptr->energy = 1;
	ptr->useFunc = AxeVerb_use;
}

static void  UseStoneAxeInitializeFunc(VERBDESCRIPTOR* ptr)
{
	assert(ptr);
	ptr->common.type.verbType = VERB_USE_WOOD_AXE;
	ptr->common.pattern = PATTERN_AXE;
	ptr->common.foreground = COLOR_DARK_SILVER;
	ptr->common.background = COLOR_TRANSPARENT;
	ptr->item = ITEM_STONE_AXE;
	ptr->caption = "Use Stone Axe";
	ptr->energy = 1;
	ptr->useFunc = AxeVerb_use;
}

static void  UseWoodPickInitializeFunc(VERBDESCRIPTOR* ptr)
{
	assert(ptr);
	ptr->common.type.verbType = VERB_USE_WOOD_PICK;
	ptr->common.pattern = PATTERN_PICKAXE;
	ptr->common.foreground = COLOR_DARK_COPPER;
	ptr->common.background = COLOR_TRANSPARENT;
	ptr->item = ITEM_WOOD_PICK;
	ptr->caption = "Use Wooden Pick";
	ptr->energy = 1;
	ptr->useFunc = PickVerb_use;
}

static void  UseStonePickInitializeFunc(VERBDESCRIPTOR* ptr)
{
	assert(ptr);
	ptr->common.type.verbType = VERB_USE_STONE_PICK;
	ptr->common.pattern = PATTERN_PICKAXE;
	ptr->common.foreground = COLOR_DARK_SILVER;
	ptr->common.background = COLOR_TRANSPARENT;
	ptr->item = ITEM_STONE_PICK;
	ptr->caption = "Use Stone Pick";
	ptr->energy = 1;
	ptr->useFunc = PickVerb_use;
}

static void  UseWoodShovelInitializeFunc(VERBDESCRIPTOR* ptr)
{
	assert(ptr);
	ptr->common.type.verbType = VERB_USE_WOOD_SHOVEL;
	ptr->common.pattern = PATTERN_SHOVEL;
	ptr->common.foreground = COLOR_DARK_COPPER;
	ptr->common.background = COLOR_TRANSPARENT;
	ptr->item = ITEM_WOOD_SHOVEL;
	ptr->caption = "Use Wooden Shovel";
	ptr->energy = 1;
	ptr->useFunc = ShovelVerb_use;
}

static void  UseStoneShovelInitializeFunc(VERBDESCRIPTOR* ptr)
{
	assert(ptr);
	ptr->common.type.verbType = VERB_USE_STONE_SHOVEL;
	ptr->common.pattern = PATTERN_SHOVEL;
	ptr->common.foreground = COLOR_DARK_SILVER;
	ptr->common.background = COLOR_TRANSPARENT;
	ptr->item = ITEM_STONE_SHOVEL;
	ptr->caption = "Use Stone Shovel";
	ptr->energy = 1;
	ptr->useFunc = ShovelVerb_use;
}

static void  UseWoodSpearInitializeFunc(VERBDESCRIPTOR* ptr)
{
	assert(ptr);
	ptr->common.type.verbType = VERB_USE_WOOD_SPEAR;
	ptr->common.pattern = PATTERN_SPEAR;
	ptr->common.foreground = COLOR_DARK_COPPER;
	ptr->common.background = COLOR_TRANSPARENT;
	ptr->item = ITEM_WOOD_SPEAR;
	ptr->caption = "Use Wooden Spear";
	ptr->energy = 1;
	ptr->useFunc = SpearVerb_use;
}

static void  EatCarnelianBerryInitializeFunc(VERBDESCRIPTOR* ptr)
{
	assert(ptr);
	ptr->common.type.verbType = VERB_EAT_CARNELIAN_BERRY;
	ptr->common.pattern = PATTERN_BERRIES;
	ptr->common.foreground = COLOR_CARNELIAN;
	ptr->common.background = COLOR_TRANSPARENT;
	ptr->item = ITEM_CARNELIAN_BERRY;
	ptr->caption = "Eat Carnelian Berries";
	ptr->useFunc = EatBerryVerb_use;
}

static void  EatCopperBerryInitializeFunc(VERBDESCRIPTOR* ptr)
{
	assert(ptr);
	ptr->common.type.verbType = VERB_EAT_COPPER_BERRY;
	ptr->common.pattern = PATTERN_BERRIES;
	ptr->common.foreground = COLOR_COPPER;
	ptr->common.background = COLOR_TRANSPARENT;
	ptr->item = ITEM_COPPER_BERRY;
	ptr->caption = "Eat Copper Berries";
	ptr->useFunc = EatBerryVerb_use;
}

static void  EatGoldBerryInitializeFunc(VERBDESCRIPTOR* ptr)
{
	assert(ptr);
	ptr->common.type.verbType = VERB_EAT_GOLD_BERRY;
	ptr->common.pattern = PATTERN_BERRIES;
	ptr->common.foreground = COLOR_GOLD;
	ptr->common.background = COLOR_TRANSPARENT;
	ptr->item = ITEM_GOLD_BERRY;
	ptr->caption = "Eat GOld Berries";
	ptr->useFunc = EatBerryVerb_use;
}

static void  EatJadeBerryInitializeFunc(VERBDESCRIPTOR* ptr)
{
	assert(ptr);
	ptr->common.type.verbType = VERB_EAT_JADE_BERRY;
	ptr->common.pattern = PATTERN_BERRIES;
	ptr->common.foreground = COLOR_JADE;
	ptr->common.background = COLOR_TRANSPARENT;
	ptr->item = ITEM_JADE_BERRY;
	ptr->caption = "Eat Jade Berries";
	ptr->useFunc = EatBerryVerb_use;
}

static void  EatTurquoiseBerryInitializeFunc(VERBDESCRIPTOR* ptr)
{
	assert(ptr);
	ptr->common.type.verbType = VERB_EAT_TURQUOISE_BERRY;
	ptr->common.pattern = PATTERN_BERRIES;
	ptr->common.foreground = COLOR_TURQUOISE;
	ptr->common.background = COLOR_TRANSPARENT;
	ptr->item = ITEM_TURQUOISE_BERRY;
	ptr->caption = "Eat Turquoise Berries";
	ptr->useFunc = EatBerryVerb_use;
}

static void  EatRubyBerryInitializeFunc(VERBDESCRIPTOR* ptr)
{
	assert(ptr);
	ptr->common.type.verbType = VERB_EAT_RUBY_BERRY;
	ptr->common.pattern = PATTERN_BERRIES;
	ptr->common.foreground = COLOR_RUBY;
	ptr->common.background = COLOR_TRANSPARENT;
	ptr->item = ITEM_RUBY_BERRY;
	ptr->caption = "Eat Ruby Berries";
	ptr->useFunc = EatBerryVerb_use;
}

static void  EatAmethystBerryInitializeFunc(VERBDESCRIPTOR* ptr)
{
	assert(ptr);
	ptr->common.type.verbType = VERB_EAT_AMETHYST_BERRY;
	ptr->common.pattern = PATTERN_BERRIES;
	ptr->common.foreground = COLOR_AMETHYST;
	ptr->common.background = COLOR_TRANSPARENT;
	ptr->item = ITEM_AMETHYST_BERRY;
	ptr->caption = "Eat Amethyst Berries";
	ptr->useFunc = EatBerryVerb_use;
}

static void  EatSilverBerryInitializeFunc(VERBDESCRIPTOR* ptr)
{
	assert(ptr);
	ptr->common.type.verbType = VERB_EAT_SILVER_BERRY;
	ptr->common.pattern = PATTERN_BERRIES;
	ptr->common.foreground = COLOR_SILVER;
	ptr->common.background = COLOR_TRANSPARENT;
	ptr->item = ITEM_SILVER_BERRY;
	ptr->caption = "Eat Silver Berries";
	ptr->useFunc = EatBerryVerb_use;
}

static void PlantBerryInitializeFunc(VERBDESCRIPTOR* ptr)
{
	assert(ptr);
	ptr->common.type.verbType = VERB_PLANT_BERRY;
	ptr->common.pattern = PATTERN_BERRYSEED;
	ptr->common.foreground = COLOR_DARK_COPPER;
	ptr->common.background = COLOR_TRANSPARENT;
	ptr->item = ITEM_BERRY_SEED;
	ptr->caption = "Plant Berry Seed";
	ptr->useFunc = PlantSeedVerb_use;
}

static void PlantAcornInitializeFunc(VERBDESCRIPTOR* ptr)
{
	assert(ptr);
	ptr->common.type.verbType = VERB_PLANT_ACORN;
	ptr->common.pattern = PATTERN_ACORN;
	ptr->common.foreground = COLOR_DARK_COPPER;
	ptr->common.background = COLOR_TRANSPARENT;
	ptr->item = ITEM_ACORN;
	ptr->caption = "Plant Acorn";
	ptr->useFunc = PlantAcornVerb_use;
}

static void PlaceGravelInitializeFunc(VERBDESCRIPTOR* ptr)
{
	assert(ptr);
	ptr->common.type.verbType = VERB_PLACE_GRAVEL;
	ptr->common.pattern = PATTERN_PILE;
	ptr->common.foreground = COLOR_DARK_SILVER;
	ptr->common.background = COLOR_TRANSPARENT;
	ptr->item = ITEM_GRAVEL;
	ptr->caption = "Place Gravel";
	ptr->useFunc = PlaceGravelVerb_use;
}

static VerbDescriptorInitializeFunc verbDescriptorInitializeFuncs[VERB_COUNT] =
{
	//VERB_LOOK,
	LookInitializeFunc,
	//VERB_TAKE,
	PickBerryInitializeFunc,
	TakeLogInitializeFunc,
	//VERB_TALK,
	TalkInitializeFunc,

	//VERB_USE_WOOD_AXE,
	UseWoodAxeInitializeFunc,
	//VERB_USE_STONE_AXE,
	UseStoneAxeInitializeFunc,

	//VERB_USE_WOOD_PICK,
	UseWoodPickInitializeFunc,
	//VERB_USE_STONE_PICK,
	UseStonePickInitializeFunc,

	//VERB_USE_WOOD_SHOVEL,
	UseWoodShovelInitializeFunc,
	//VERB_USE_STONE_SHOVEL,
	UseStoneShovelInitializeFunc,

	//VERB_USE_WOOD_SPEAR,
	UseWoodSpearInitializeFunc,

	//VERB_PLANT_BERRY,
	PlantBerryInitializeFunc,
	//VERB_PLANT_ACORN,
	PlantAcornInitializeFunc,
	//VERB_PLACE_GRAVEL,
	PlaceGravelInitializeFunc,

	//VERB_EAT_CARNELIAN_BERRY,
	EatCarnelianBerryInitializeFunc,
	//VERB_EAT_COPPER_BERRY,
	EatCopperBerryInitializeFunc,
	//VERB_EAT_GOLD_BERRY,
	EatGoldBerryInitializeFunc,
	//VERB_EAT_JADE_BERRY,
	EatJadeBerryInitializeFunc,
	//VERB_EAT_TURQUOISE_BERRY,
	EatTurquoiseBerryInitializeFunc,
	//VERB_EAT_RUBY_BERRY,
	EatRubyBerryInitializeFunc,
	//VERB_EAT_AMETHYST_BERRY,
	EatAmethystBerryInitializeFunc,
	//VERB_EAT_SILVER_BERRY,
	EatSilverBerryInitializeFunc
};

void VerbDescriptors_initialize()
{
	for (VERBTYPE verb = VERB_FIRST; verb < VERB_COUNT; ++verb)
	{
		verbDescriptorInitializeFuncs[verb](VerbDescriptors_getDescriptor(verb));
	}
}

VERBDESCRIPTOR* VerbDescriptors_getDescriptor(VERBTYPE verb)
{
	assert(verb >= VERB_FIRST && verb < VERB_COUNT);
	return &verbDescriptors[verb];
}

int VerbDescriptors_getEnergy(VERBTYPE verb)
{
	assert(verb >= VERB_FIRST && verb < VERB_COUNT);
	return VerbDescriptor_getEnergy(VerbDescriptors_getDescriptor(verb));
}

ITEMTYPE VerbDescriptors_getItem(VERBTYPE verb)
{
	assert(verb >= VERB_FIRST && verb < VERB_COUNT);
	return VerbDescriptor_getItem(VerbDescriptors_getDescriptor(verb));
}
