#include "View_internals.h"
#include "PixelPlotter_internals.h"
#include "PatternPlotter.h"
#include "Views.h"
#include "Game.h"
#include "Constants.h"
#include "TerrainDescriptors.h"
#include "CreatureDescriptors.h"
#include "ItemDescriptors.h"
#include "Direction.h"
#include <assert.h>
#include "Utility.h"
#include <stdio.h>
#include "TextPlotter.h"
#include "Recipes.h"
#include <string.h>

#define CRAFTSCREEN_OFFSET_X (0)
#define CRAFTSCREEN_OFFSET_Y (0)
#define CRAFTSCREEN_WIDTH (SCALE_WIDTH(SCALE_SMALL))
#define CRAFTSCREEN_HEIGHT (SCALE_HEIGHT(SCALE_SMALL))
#define CRAFTSCREEN_ROWS ((CRAFTSCREEN_HEIGHT)/(PATTERN_HEIGHT))
#define CRAFTSCREEN_COLUMNS  ((CRAFTSCREEN_WIDTH)/(PATTERN_WIDTH))

#define CRAFTSCREEN_RECIPE_OFFSET_X (CRAFTSCREEN_OFFSET_X)
#define CRAFTSCREEN_RECIPE_OFFSET_Y (CRAFTSCREEN_OFFSET_Y + PATTERN_HEIGHT)

#define CRAFTSCREEN_INVENTORY_OFFSET_X (CRAFTSCREEN_OFFSET_X + CRAFTSCREEN_COLUMNS * PATTERN_WIDTH * 3 / 4)
#define CRAFTSCREEN_INVENTORY_OFFSET_Y (CRAFTSCREEN_OFFSET_Y + PATTERN_HEIGHT)

#define CRAFTSCREEN_SCROLLER_LIST_SIZE (CRAFTSCREEN_ROWS - 2)
#define CRAFTSCREEN_SCROLLER_HEIGHT (CRAFTSCREEN_SCROLLER_LIST_SIZE * PATTERN_HEIGHT)

void CraftScreen_initialize(union View* ptr)
{
	assert(ptr);
	PixelPlotterState_initialize(View_getPixelPlotterState(ptr),
		CRAFTSCREEN_OFFSET_X,
		CRAFTSCREEN_OFFSET_Y,
		SCALE_SMALL,
		SCALE_SMALL,
		1,
		CRAFTSCREEN_WIDTH,
		CRAFTSCREEN_HEIGHT);
	ptr->craftScreen.viewCommon.viewType = VIEW_CRAFT_SCREEN;
	ptr->craftScreen.viewCommon.enabled = 0;

	for (CRAFTSCREENSCROLLERTYPE scroller = CRAFTSCREENSCROLLER_FIRST; scroller < CRAFTSCREENSCROLLER_COUNT; ++scroller)
	{
		Scroller_initialize(CraftScreen_getScroller(View_getCraftScreen(ptr), scroller), 0, CRAFTSCREEN_SCROLLER_LIST_SIZE);
	}

	ptr->craftScreen.currentScroller = CRAFTSCREENSCROLLER_FIRST;
}

void CraftScreen_draw(union View* ptr)
{
	//TODO: need scrollbars
	assert(ptr);
	PIXELPLOTTERSTATE old;
	PixelPlotter_getState(&old);
	PixelPlotter_setState(View_getPixelPlotterState(ptr));
	char buffer[10];

	PixelPlotter_plotRect(0, 0, CRAFTSCREEN_WIDTH, CRAFTSCREEN_HEIGHT, COLOR_DARK_AMETHYST);

	TextPlotter_plotText(CRAFTSCREEN_RECIPE_OFFSET_X, 0, "Craft:", COLOR_SILVER, COLOR_TRANSPARENT);
	TextPlotter_plotText(CRAFTSCREEN_INVENTORY_OFFSET_X, 0, "Inventory:", COLOR_SILVER, COLOR_TRANSPARENT);

	TextPlotter_plotText(0, CRAFTSCREEN_HEIGHT - PATTERN_HEIGHT, "  back    select   craft    switch", COLOR_SILVER, COLOR_TRANSPARENT);
	TextPlotter_plotText(0 * PATTERN_WIDTH, CRAFTSCREEN_HEIGHT - PATTERN_HEIGHT, "C", COLOR_DARK_SILVER, COLOR_SILVER);
	TextPlotter_plotText(7 * PATTERN_WIDTH, CRAFTSCREEN_HEIGHT - PATTERN_HEIGHT, "WS", COLOR_DARK_SILVER, COLOR_SILVER);
	TextPlotter_plotText(17 * PATTERN_WIDTH, CRAFTSCREEN_HEIGHT - PATTERN_HEIGHT, "E", COLOR_DARK_SILVER, COLOR_SILVER);
	TextPlotter_plotText(25 * PATTERN_WIDTH, CRAFTSCREEN_HEIGHT - PATTERN_HEIGHT, ",.", COLOR_DARK_SILVER, COLOR_SILVER);

	CRAFTSCREEN* craftScreen = View_getCraftScreen(ptr);
	SCROLLER* inventoryScroller = CraftScreen_getScroller(craftScreen, CRAFTSCREENSCROLLER_INVENTORY);
	TAGON* tagon = Game_getTagon();
	if (Scroller_getItemCount(inventoryScroller)==0)
	{
		TextPlotter_plotText(CRAFTSCREEN_INVENTORY_OFFSET_X, PATTERN_HEIGHT, "(nothing)", COLOR_SILVER, COLOR_TRANSPARENT);
	}
	//draw inventory
	for (int row = 0, plotY = CRAFTSCREEN_INVENTORY_OFFSET_Y; row < Utility_minimum(CRAFTSCREEN_ROWS,Scroller_getItemCount(inventoryScroller)); ++row, plotY += PATTERN_HEIGHT)
	{
		int plotX = CRAFTSCREEN_INVENTORY_OFFSET_X;
		PatternPlotter_plotPattern(plotX, plotY, PATTERN_EMPTY, COLOR_ONYX, COLOR_ONYX);
		int effectiveRow = row + Scroller_getScroll(inventoryScroller);
		if (effectiveRow == Scroller_getIndex(inventoryScroller))
		{
			PatternPlotter_plotPattern(plotX, plotY, PATTERN_BOX, (CraftScreen_getCurrentScroller(craftScreen)==CRAFTSCREENSCROLLER_INVENTORY)?(COLOR_LIGHTEST):(COLOR_DARKEST), COLOR_TRANSPARENT);
		}
		ITEMTYPE item = ptr->craftScreen.inventoryItems[effectiveRow];
		ITEMDESCRIPTOR* descriptor = ItemDescriptors_getDescriptor(item);
		PatternPlotter_plotPattern(plotX, plotY, ItemDescriptor_getPattern(descriptor), ItemDescriptor_getForeground(descriptor), ItemDescriptor_getBackground(descriptor));
		snprintf(buffer, 10, "x%d", Tagon_getInventoryCount(tagon, item));
		TextPlotter_plotText(plotX + PATTERN_WIDTH, plotY, buffer, COLOR_SILVER, COLOR_TRANSPARENT);
	}
	if (Scroller_canScrollUp(inventoryScroller))
	{
		TextPlotter_plotText(CRAFTSCREEN_WIDTH - PATTERN_WIDTH, CRAFTSCREEN_INVENTORY_OFFSET_Y, "\x18", COLOR_DARK_SILVER, COLOR_SILVER);
	}
	if (Scroller_canScrollDown(inventoryScroller))
	{
		TextPlotter_plotText(CRAFTSCREEN_WIDTH - PATTERN_WIDTH, CRAFTSCREEN_INVENTORY_OFFSET_Y + (CRAFTSCREEN_SCROLLER_LIST_SIZE - 1) * PATTERN_HEIGHT, "\x19", COLOR_DARK_SILVER, COLOR_SILVER);
	}

	SCROLLER* recipeScroller = CraftScreen_getScroller(craftScreen, CRAFTSCREENSCROLLER_RECIPE);
	if (Scroller_getItemCount(recipeScroller) == 0)
	{
		TextPlotter_plotText(CRAFTSCREEN_RECIPE_OFFSET_X, PATTERN_HEIGHT, "(nothing)", COLOR_SILVER, COLOR_TRANSPARENT);
	}
	for (int row = 0, plotY = CRAFTSCREEN_INVENTORY_OFFSET_Y; row < Utility_minimum(CRAFTSCREEN_ROWS, Scroller_getItemCount(recipeScroller)); ++row, plotY += PATTERN_HEIGHT)
	{
		int plotX = CRAFTSCREEN_RECIPE_OFFSET_X;
		int effectiveRow = row + Scroller_getScroll(recipeScroller);
		int hiliteRow = (effectiveRow == Scroller_getIndex(recipeScroller));

		RECIPE* recipe = Recipes_getRecipe(ptr->craftScreen.recipes[effectiveRow]);
		INVENTORY* inventory = Recipe_getOutput(recipe);
		for (ITEMTYPE item = ITEM_FIRST; item < ITEM_COUNT; ++item)
		{
			if (Inventory_hasCount(inventory,item))
			{
				snprintf(buffer, 10, " x%d", Inventory_getCount(inventory, item));
				TextPlotter_plotText(plotX, plotY, buffer, COLOR_SILVER, COLOR_TRANSPARENT);
				PatternPlotter_plotPattern(plotX, plotY, PATTERN_EMPTY, COLOR_ONYX, COLOR_ONYX);
				if (hiliteRow)
				{
					PatternPlotter_plotPattern(plotX, plotY, PATTERN_BOX, (CraftScreen_getCurrentScroller(craftScreen) == CRAFTSCREENSCROLLER_RECIPE) ? (COLOR_LIGHTEST) : (COLOR_DARKEST), COLOR_TRANSPARENT);
				}
				ITEMDESCRIPTOR* descriptor = ItemDescriptors_getDescriptor(item);
				PatternPlotter_plotPattern(plotX, plotY, ItemDescriptor_getPattern(descriptor), ItemDescriptor_getForeground(descriptor), ItemDescriptor_getBackground(descriptor));

				plotX += (PATTERN_WIDTH * (int)strlen(buffer));
			}
		}

		TextPlotter_plotText(plotX, plotY, " \x1b ", COLOR_TURQUOISE, COLOR_TRANSPARENT);
		plotX += (PATTERN_WIDTH * (int)strlen(" \x1b "));

		inventory = Recipe_getInput(recipe);
		for (ITEMTYPE item = ITEM_FIRST; item < ITEM_COUNT; ++item)
		{
			if (Inventory_hasCount(inventory, item))
			{
				snprintf(buffer, 10, " x%d", Inventory_getCount(inventory, item));
				TextPlotter_plotText(plotX, plotY, buffer, COLOR_SILVER, COLOR_TRANSPARENT);
				PatternPlotter_plotPattern(plotX, plotY, PATTERN_EMPTY, COLOR_ONYX, COLOR_ONYX);
				ITEMDESCRIPTOR* descriptor = ItemDescriptors_getDescriptor(item);
				PatternPlotter_plotPattern(plotX, plotY, ItemDescriptor_getPattern(descriptor), ItemDescriptor_getForeground(descriptor), ItemDescriptor_getBackground(descriptor));

				plotX += (PATTERN_WIDTH * (int)strlen(buffer));
			}
		}
	}
	if (Scroller_canScrollUp(recipeScroller))
	{
		TextPlotter_plotText(CRAFTSCREEN_INVENTORY_OFFSET_X - PATTERN_WIDTH, CRAFTSCREEN_RECIPE_OFFSET_Y, "\x18", COLOR_DARK_SILVER, COLOR_SILVER);
	}
	if (Scroller_canScrollDown(recipeScroller))
	{
		TextPlotter_plotText(CRAFTSCREEN_INVENTORY_OFFSET_X - PATTERN_WIDTH, CRAFTSCREEN_RECIPE_OFFSET_Y + (CRAFTSCREEN_SCROLLER_LIST_SIZE - 1) * PATTERN_HEIGHT, "\x19", COLOR_DARK_SILVER, COLOR_SILVER);
	}


	PixelPlotter_setState(&old);
}

int CraftScreen_process(union View* ptr, VIEWCOMMANDTYPE command)
{
	assert(ptr);
	CRAFTSCREEN* craftScreen = View_getCraftScreen(ptr);
	TAGON* tagon = Game_getTagon();
	RECIPE* recipe;
	switch (command)
	{
	case VIEWCOMMAND_BUTTON_A:
		if (Scroller_getItemCount(CraftScreen_getScroller(craftScreen, CRAFTSCREENSCROLLER_RECIPE)) > 0)
		{
			recipe = Recipes_getRecipe(craftScreen->recipes[Scroller_getIndex(CraftScreen_getScroller(craftScreen, CRAFTSCREENSCROLLER_RECIPE))]);
			Recipe_craft(recipe, Tagon_getInventory(tagon));
			CraftScreen_refresh(craftScreen);
		}
		return 1;
	case VIEWCOMMAND_MOVE_DOWN:
		Scroller_nextIndex(CraftScreen_getScroller(craftScreen, CraftScreen_getCurrentScroller(craftScreen)));
		Scroller_refresh(CraftScreen_getScroller(craftScreen, CraftScreen_getCurrentScroller(craftScreen)));
		return 1;
	case VIEWCOMMAND_MOVE_UP:
		Scroller_previousIndex(CraftScreen_getScroller(craftScreen, CraftScreen_getCurrentScroller(craftScreen)));
		Scroller_refresh(CraftScreen_getScroller(craftScreen, CraftScreen_getCurrentScroller(craftScreen)));
		return 1;
	case VIEWCOMMAND_BUTTON_NEXT:
	case VIEWCOMMAND_BUTTON_PREVIOUS:
		CraftScreen_toggleCurrentScroller(craftScreen);
		return 1;
	case VIEWCOMMAND_BUTTON_Y:
		Tagon_postInteract(tagon);
		Views_disable(VIEW_CRAFT_SCREEN);
		return 1;
	default:
		return 1;
	}
}

void CraftScreen_refresh(CRAFTSCREEN* ptr)
{
	assert(ptr);
	TAGON* tagon = Game_getTagon();
	//refresh inventory
	ITEMTYPE inventoryIndex = ITEM_FIRST;
	for (ITEMTYPE iterator = ITEM_FIRST; iterator < ITEM_COUNT; ++iterator)
	{
		if (Tagon_hasInventoryCount(tagon, iterator))
		{
			ptr->inventoryItems[inventoryIndex++] = iterator;
		}
	}
	Scroller_setItemCount(CraftScreen_getScroller(ptr, CRAFTSCREENSCROLLER_INVENTORY), inventoryIndex);
	//refresh recipes
	RECIPETYPE recipeIndex = RECIPE_FIRST;
	for (RECIPETYPE iterator = RECIPE_FIRST; iterator < RECIPE_COUNT; ++iterator)
	{
		RECIPE* recipe = Recipes_getRecipe(iterator);
		if (Recipe_canCraft(recipe, Tagon_getInventory(tagon)))
		{
			ptr->recipes[recipeIndex++] = iterator;
		}
	}
	Scroller_setItemCount(CraftScreen_getScroller(ptr, CRAFTSCREENSCROLLER_RECIPE), recipeIndex);
}

SCROLLER* CraftScreen_getScroller(CRAFTSCREEN* ptr, CRAFTSCREENSCROLLERTYPE scrollerType)
{
	assert(ptr);
	assert(scrollerType >= CRAFTSCREENSCROLLER_FIRST && scrollerType < CRAFTSCREENSCROLLER_COUNT);
	return &(ptr->scrollers[scrollerType]);
}

CRAFTSCREENSCROLLERTYPE CraftScreen_getCurrentScroller(CRAFTSCREEN* ptr)
{
	assert(ptr);
	return ptr->currentScroller;
}

void CraftScreen_toggleCurrentScroller(CRAFTSCREEN* ptr)
{
	assert(ptr);
	ptr->currentScroller = (ptr->currentScroller + 1) % CRAFTSCREENSCROLLER_COUNT;
}

