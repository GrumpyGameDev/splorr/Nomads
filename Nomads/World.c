#include "World.h"
#include "Generators.h"
#include "Terrain.h"
#include "TerrainDescriptors.h"
#include <stdlib.h>
#include "Creature.h"
#include "CreatureDescriptors.h"
#include <string.h>
#include "World_internals.h"
#include <assert.h>

static int creaturePopulation[CREATURE_COUNT]=
{
	1
};

void World_initialize(WORLD* ptr)
{
	assert(ptr);
	//set up terrain
	for (int column = 0; column < MAP_COLUMNS; ++column)
	{
		for (int row = 0; row < MAP_ROWS; ++row)
		{
			MAPCELL* cell = Map_getMapCell(World_getMap(ptr), column, row);
			TERRAINTYPE terrainType = Generator_generate(Generators_getGenerator(GENERATOR_TERRAINTYPE));
			Terrain_initialize(MapCell_getTerrain(cell), TerrainDescriptors_getDescriptor(terrainType));
			Creature_clear(MapCell_getCreature(cell));
		}
	}
	//populate the world
	for (CREATURETYPE creatureType = CREATURE_FIRST; creatureType < CREATURE_COUNT; ++creatureType)
	{
		int count = creaturePopulation[creatureType];
		while (count > 0)
		{
			int column = rand() % MAP_COLUMNS;
			int row = rand() % MAP_ROWS;

			MAPCELL* cell = Map_getMapCell(World_getMap(ptr), column, row);
			TERRAINTYPE terrainType = Terrain_getType(MapCell_getTerrain(cell));
			TERRAINDESCRIPTOR* descriptor = TerrainDescriptors_getDescriptor(terrainType);
			if (TerrainDescriptor_canSpawnCreature(descriptor, creatureType))
			{
				Creature_initialize(MapCell_getCreature(cell), CreatureDescriptors_getDescriptor(creatureType));
				if (creatureType == CREATURE_TAGON)
				{
					Avatar_initialize(World_getAvatar(ptr), column, row, DIRECTION_COUNT);
				}
				count--;
			}
		}
	}
	for(int row = 0; row< MESSAGEFEED_ROWS; ++ row)
	{
		ptr->messageFeed.lines[row].color=COLOR_SILVER;
		strncpy_s(ptr->messageFeed.lines[row].text, MESSAGEFEED_COLUMNS + 1,"", MESSAGEFEED_COLUMNS + 1);
	}
}

void World_doTimers(WORLD* ptr)
{
	assert(ptr);
	Avatar_doTimers(World_getAvatar(ptr));
	Map_doTimers(World_getMap(ptr));
}

TAGON* World_getTagon(WORLD* ptr)
{
	assert(ptr);
	AVATAR* avatar = World_getAvatar(ptr);
	MAPCELL* cell = Map_getMapCell(World_getMap(ptr),avatar->x,avatar->y);
	return Creature_getTagon(MapCell_getCreature(cell));
}

INVENTORY* World_getGroundInventory(WORLD* ptr)
{
	assert(ptr);
	AVATAR* avatar = World_getAvatar(ptr);
	MAPCELL* cell = Map_getMapCell(World_getMap(ptr), avatar->x, avatar->y);
	return MapCell_getInventory(cell);
}

AVATAR* World_getAvatar(WORLD* ptr)
{
	assert(ptr);
	return &(ptr->avatar);
}

MAP* World_getMap(WORLD* ptr)
{
	assert(ptr);
	return &(ptr->map);
}

MESSAGEFEED* World_getMessageFeed(WORLD* ptr)
{
	assert(ptr);
	return &(ptr->messageFeed);
}

void World_writeMessage(WORLD* ptr, const char* message, COLORTYPE color)
{
	MessageFeed_writeMessage(World_getMessageFeed(ptr), message, color);
}



