#ifndef STATISTIC_TYPE_H
#define STATISTIC_TYPE_H

#ifdef __cplusplus
extern "C" {
#endif

	typedef enum {
		STATISTIC_ENERGY,
		STATISTIC_HEALTH,
		STATISTIC_COUNT,
		STATISTIC_FIRST = STATISTIC_ENERGY
	} STATISTICTYPE;

#ifdef __cplusplus
}
#endif

#endif
