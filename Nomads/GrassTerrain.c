#include "GrassTerrain_internals.h"
#include "Terrain_internals.h"
#include <assert.h>

void GrassTerrain_initialize(union Terrain* ptr, union TerrainDescriptor* descriptor)
{
	assert(ptr);
	assert(descriptor);
	ptr->common.type = TERRAIN_GRASS;
}

void StumpTerrain_initialize(union Terrain* ptr, union TerrainDescriptor* descriptor)//TODO: move me to stump.c
{
	assert(ptr);
	assert(descriptor);
	ptr->common.type = TERRAIN_STUMP;
}

void RubbleTerrain_initialize(union Terrain* ptr, union TerrainDescriptor* descriptor)//TODO: move me to rubble.c
{
	assert(ptr);
	assert(descriptor);
	ptr->common.type = TERRAIN_RUBBLE;
}

void RabbitHole_initialize(union Terrain* ptr, union TerrainDescriptor* descriptor)//TODO: move me
{
	assert(ptr);
	assert(descriptor);
	ptr->common.type = TERRAIN_RABBIT_HOLE;
}

