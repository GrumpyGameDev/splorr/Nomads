#ifndef INVENTORY_H
#define INVENTORY_H

#ifdef __cplusplus
extern "C" {
#endif

#include "ItemType.h"

typedef struct Inventory  INVENTORY;

void Inventory_clear(INVENTORY* ptr);
void Inventory_setCount(INVENTORY* ptr, ITEMTYPE item, int count);
void Inventory_changeCountBy(INVENTORY* ptr, ITEMTYPE item, int delta);
int Inventory_getCount(INVENTORY* ptr, ITEMTYPE item);
int Inventory_getTotalCount(INVENTORY* ptr);
int Inventory_hasCount(INVENTORY* ptr, ITEMTYPE item);
void Inventory_add(INVENTORY* ptr, INVENTORY* first, INVENTORY* second);
void Inventory_subtract(INVENTORY* ptr, INVENTORY* first, INVENTORY* second);
int Inventory_contains(INVENTORY* ptr, INVENTORY* subset);
ITEMTYPE Inventory_getFirstItem(INVENTORY* ptr);

#ifdef __cplusplus
}
#endif

#endif

