#ifndef RECIPE_INTERNALS_H
#define RECIPE_INTERNALS_H

#ifdef __cplusplus
extern "C" {
#endif

#include "Inventory_internals.h"
#include "Recipe.h"

	struct Recipe {
		INVENTORY input;
		INVENTORY output;
	};

#ifdef __cplusplus
}
#endif

#endif

