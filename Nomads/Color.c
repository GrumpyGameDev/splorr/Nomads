#include "Color_internal.h"
#include <assert.h>

unsigned char Color_getRed(COLOR* ptr)
{
	assert(ptr);
	return ptr->red;
}

unsigned char Color_getGreen(COLOR* ptr)
{
	assert(ptr);
	return ptr->green;
}

unsigned char Color_getBlue(COLOR* ptr)
{
	assert(ptr);
	return ptr->blue;
}

unsigned char Color_getAlpha(COLOR* ptr)
{
	assert(ptr);
	return ptr->alpha;
}

