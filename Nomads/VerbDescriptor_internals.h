#ifndef VERB_DESCRIPTOR_INTERNALS_H
#define VERB_DESCRIPTOR_INTERNALS_H

#ifdef __cplusplus
extern "C" {
#endif

#include "VerbDescriptor.h"
#include "DescriptorCommon_internals.h"

typedef void(*VerbDescriptorUseFunc)(VERBDESCRIPTOR*, TAGON*);

struct VerbDescriptor {
	DESCRIPTORCOMMON common;
	const char* caption;
	ITEMTYPE item;
	int energy;
	VerbDescriptorUseFunc useFunc;
};

#ifdef __cplusplus
}
#endif

#endif

