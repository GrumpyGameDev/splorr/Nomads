#ifndef RECIPES_H
#define RECIPES_H

#ifdef __cplusplus
extern "C" {
#endif

#include "RecipeType.h"
#include "Recipe.h"

	void Recipes_initialize();
	RECIPE* Recipes_getRecipe(RECIPETYPE recipeType);

#ifdef __cplusplus
}
#endif

#endif

