#include "View_internals.h"
#include "PixelPlotter_internals.h"
#include "PatternPlotter.h"
#include "Views.h"
#include "Game.h"
#include "Constants.h"
#include "TerrainDescriptors.h"
#include "CreatureDescriptors.h"
#include "ItemDescriptors.h"
#include "Direction.h"
#include <assert.h>

#define MAPSCREEN_COLUMNS (SCALE_WIDTH(SCALE_LARGE)/PATTERN_WIDTH + 1)
#define MAPSCREEN_ROWS (SCALE_HEIGHT(SCALE_LARGE)/PATTERN_HEIGHT + 1)
#define MAPSCREEN_WIDTH (PATTERN_WIDTH * MAPSCREEN_COLUMNS)
#define MAPSCREEN_HEIGHT (PATTERN_HEIGHT * MAPSCREEN_ROWS)

void MapScreen_initialize(union View* ptr)
{
	assert(ptr);
	PixelPlotterState_initialize(View_getPixelPlotterState(ptr),
	-PATTERN_WIDTH/2,
	-PATTERN_HEIGHT/4,
	SCALE_LARGE,
	SCALE_LARGE,
	1,
	MAPSCREEN_WIDTH,
	MAPSCREEN_HEIGHT);
	ptr->mapScreen.viewCommon.viewType = VIEW_MAP_SCREEN;
	ptr->mapScreen.viewCommon.enabled = 1;
	Game_initialize();
}

void MapScreen_draw(union View* ptr)
{
	assert(ptr);
	PIXELPLOTTERSTATE old;
	PixelPlotter_getState(&old);
	PixelPlotter_setState(View_getPixelPlotterState(ptr));

	WORLD* world = Game_getWorld();
	AVATAR* avatar = World_getAvatar(world);
	int cursorX = Direction_getNextX(Avatar_getX(avatar), Avatar_getY(avatar), Avatar_getFacing(avatar));
	int cursorY = Direction_getNextY(Avatar_getX(avatar), Avatar_getY(avatar), Avatar_getFacing(avatar));
	for (int x = 0, mapX = Avatar_getX(avatar) - MAPSCREEN_COLUMNS / 2, plotX=0; x < MAPSCREEN_COLUMNS; ++x, ++mapX, plotX+=PATTERN_WIDTH)
	{
		for(int y = 0, mapY = Avatar_getY(avatar) - MAPSCREEN_ROWS / 2, plotY=0; y < MAPSCREEN_COLUMNS; ++y, ++mapY, plotY+=PATTERN_HEIGHT)
		{
			MAPCELL* cell = Map_getMapCell(World_getMap(world), mapX, mapY);

			TERRAINDESCRIPTOR* terrainDescriptor = TerrainDescriptors_getDescriptor(Terrain_getType(MapCell_getTerrain(cell)));
			PatternPlotter_plotPattern(plotX, plotY, TerrainDescriptor_getPattern(terrainDescriptor), TerrainDescriptor_getForeground(terrainDescriptor), TerrainDescriptor_getBackground(terrainDescriptor));

			if (mapX == cursorX && mapY == cursorY)
			{
				PatternPlotter_plotPattern(plotX, plotY, PATTERN_SELECTOR, COLOR_TURQUOISE, COLOR_TRANSPARENT);
			}

			ITEMTYPE itemType = Inventory_getFirstItem(MapCell_getInventory(cell));
			if (itemType != ITEM_NONE)
			{
				ITEMDESCRIPTOR* itemDescriptor = ItemDescriptors_getDescriptor(itemType);
				PatternPlotter_plotPattern(plotX, plotY, ItemDescriptor_getPattern(itemDescriptor), ItemDescriptor_getForeground(itemDescriptor), ItemDescriptor_getBackground(itemDescriptor));
			}

			CREATURE* creatureInstance = MapCell_getCreature(cell);
			CREATURETYPE creatureType = Creature_getType(creatureInstance);
			CREATUREDESCRIPTOR* creatureDescriptor = CreatureDescriptors_getDescriptor(creatureType);
			PatternPlotter_plotPattern(plotX, plotY, CreatureDescriptor_getPattern(creatureDescriptor), CreatureDescriptor_getForeground(creatureDescriptor), CreatureDescriptor_getBackground(creatureDescriptor));
		}
	}

	PixelPlotter_setState(&old);
}

int MapScreen_process(union View* ptr, VIEWCOMMANDTYPE command)
{
	assert(ptr);
	switch (command)
	{
	case VIEWCOMMAND_MOVE_DOWN:
		Game_moveTagon(DIRECTION_SOUTH);
		return 1;
	case VIEWCOMMAND_MOVE_LEFT:
		Game_moveTagon(DIRECTION_WEST);
		return 1;
	case VIEWCOMMAND_MOVE_RIGHT:
		Game_moveTagon(DIRECTION_EAST);
		return 1;
	case VIEWCOMMAND_MOVE_UP:
		Game_moveTagon(DIRECTION_NORTH);
		return 1;
	case VIEWCOMMAND_BUTTON_START:
		Views_enable(VIEW_MAIN_MENU);
		return 1;
	case VIEWCOMMAND_BUTTON_X:
		{
			Game_tagonInteract();
		}
		return 1;
	case VIEWCOMMAND_BUTTON_A:
		Game_tagonUseVerb();
		return 1;
	case VIEWCOMMAND_BUTTON_Y:
		CraftScreen_refresh(View_getCraftScreen(Views_getView(VIEW_CRAFT_SCREEN)));
		Views_enable(VIEW_CRAFT_SCREEN);
		return 1;
	case VIEWCOMMAND_BUTTON_B:
		InventoryScreen_refresh(View_getInventoryScreen(Views_getView(VIEW_INVENTORY_SCREEN)));
		Views_enable(VIEW_INVENTORY_SCREEN);
		return 1;
	default:
		return 0;
	}
}

