#include "DirtTerrain_internals.h"
#include "Utility.h"
#include <assert.h>
#include "Terrain_internals.h"
#include "Generators.h"
#include "MapCell.h"
#include "TerrainDescriptors.h"

int DirtTerrain_getGrassTimer(DIRTTERRAIN* ptr)
{
	assert(ptr);
	return ptr->grassTimer;
}

void DirtTerrain_setGrassTimer(DIRTTERRAIN* ptr, int grassTimer)
{
	assert(ptr);
	ptr->grassTimer = grassTimer;
}

void DirtTerrain_changeGrassTimerBy(DIRTTERRAIN* ptr, int delta)
{
	assert(ptr);
	DirtTerrain_setGrassTimer(ptr, DirtTerrain_getGrassTimer(ptr) + delta);
}

void DirtTerrain_doTimer(union TerrainDescriptor* ptr, struct MapCell* cell)
{
	DIRTTERRAIN* dirt = Terrain_getDirt(MapCell_getTerrain(cell));

	DirtTerrain_changeGrassTimerBy(dirt, -1);
	if (DirtTerrain_getGrassTimer(dirt) <= 0)
	{
		TerrainDescriptor_initializeInstance(TerrainDescriptors_getDescriptor(TERRAIN_GRASS), MapCell_getTerrain(cell));
	}
}

void DirtTerrain_initialize(union Terrain* ptr, union TerrainDescriptor* descriptor)
{
	assert(ptr);
	assert(descriptor);
	ptr->common.type = TERRAIN_DIRT;
	ptr->dirt.grassTimer = Generator_generate(Generators_getGenerator(GENERATOR_DIRTTOGRASS));
}

