#ifndef CREATURE_COMMON_H
#define CREATURE_COMMON_H

#ifdef __cplusplus
extern "C" {
#endif

#include "CreatureType.h"

typedef struct CreatureCommon CREATURECOMMON;

CREATURETYPE CreatureCommon_getType(CREATURECOMMON* ptr);

#ifdef __cplusplus
}
#endif

#endif

