#ifndef VIEW_TYPE_H
#define VIEW_TYPE_H

#ifdef __cplusplus
extern "C" {
#endif

typedef enum ViewType{
	VIEW_MAP_SCREEN,
	VIEW_STATS,
	VIEW_MESSAGE_FEED,	
	VIEW_VERBTOOLBAR,
	VIEW_QUICK_HELP,
	VIEW_CRAFT_SCREEN,
	VIEW_INVENTORY_SCREEN,
	VIEW_MAIN_MENU,
	VIEW_CONFIRM_QUIT,

	VIEW_COUNT,
	VIEW_FIRST = VIEW_MAP_SCREEN
} VIEWTYPE;

#ifdef __cplusplus
}
#endif

#endif

